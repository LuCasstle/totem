// Here is where you can define configuration overrides based on the execution environment.
// Supply a key to the default export matching the NODE_ENV that you wish to target, and
// the base configuration will apply your overrides before exporting itself.

const DEV_API_ADDR = 'http://api.demo.flykke.com'
const STAGING_API_ADDR = 'http://api.staging.flykke.com'
const PROD_API_ADDR = 'http://api.flykke.com'

module.exports = {
  // ======================================================
  // Overrides when NODE_ENV === 'development'
  // ======================================================
  // NOTE: In development, we use an explicit public path when the assets
  // are served by webpack to fix this issue:
  // http://stackoverflow.com/questions/34133808/webpack-ots-parsing-error-loading-fonts/34133809#34133809
  development : (config) => ({
    compiler_public_path : `http://${config.server_host}:${config.server_port}/`,
    globals: Object.assign(config.globals, {
      '__SERVER_ADDR__': JSON.stringify(DEV_API_ADDR),
      '__FB_APP_ID__': 164796614062574
    }),
    devServer: {
      headers: {
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE, PATCH, OPTIONS",
        "Access-Control-Allow-Headers": "X-Requested-With, content-type, Authorization"
      }
    }
  }),

  // ======================================================
  // Overrides when NODE_ENV === 'production'
  // ======================================================
  production : (config) => ({
    compiler_public_path     : '/',
    compiler_fail_on_warning : false,
    compiler_hash_type       : 'chunkhash',
    compiler_devtool         : null,
    compiler_stats           : {
      chunks       : true,
      chunkModules : true,
      colors       : true
    },
    globals: Object.assign(config.globals, {
      '__SERVER_ADDR__': JSON.stringify(PROD_API_ADDR),
      '__FB_APP_ID__': 2190954004464175
    })
  }),

  staging : (config) => ({
    compiler_public_path     : '/',
    compiler_fail_on_warning : false,
    compiler_hash_type       : 'chunkhash',
    compiler_devtool         : null,
    compiler_stats           : {
      chunks       : true,
      chunkModules : true,
      colors       : true
    },
    globals: Object.assign(config.globals, {
      '__SERVER_ADDR__': JSON.stringify(STAGING_API_ADDR),
      '__FB_APP_ID__': 1467233306653058
    })
  })
}
