export const MEDIA_TYPES = {
  IMAGE: 'image',
  VIDEO: 'video'
}

export const SPECIFIC_TYPES_FOR_MEDIA = {
  [MEDIA_TYPES.IMAGE]: {
    ARTICLE: 'article',
    THUMBNAIL: 'thumbnail',
  }
}

export const MAX_SIZES = {
  [MEDIA_TYPES.IMAGE]: {
    [SPECIFIC_TYPES_FOR_MEDIA[MEDIA_TYPES.IMAGE].ARTICLE]: 1000000,  // 1 Mo
    [SPECIFIC_TYPES_FOR_MEDIA[MEDIA_TYPES.IMAGE].THUMBNAIL]: 300000, // 300 Ko
  },
  [MEDIA_TYPES.VIDEO]: 10000000 // 10 Mo
};

export const AUTHORIZED_TYPES = ['image/png', 'image/jpg']
