export const URLS = {
  facebook: 'http://res.cloudinary.com/djv5lkgnv/image/upload/v1497438501/socialicons/facebook_167_167.png',
  twitter: 'http://res.cloudinary.com/djv5lkgnv/image/upload/v1497438501/socialicons/twitter_167_167.png',
  youtube: 'http://res.cloudinary.com/djv5lkgnv/image/upload/v1497438501/socialicons/youtube_167_167.png',
  instagram: 'http://res.cloudinary.com/djv5lkgnv/image/upload/v1497438501/socialicons/instagram_167_167.png',
  email: '/assets/icons/email.png'
}
