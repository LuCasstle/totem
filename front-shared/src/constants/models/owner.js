export const OWNER_MODEL = {
  NAME: 'name',
  DISPLAY_DESCRIPTION: 'displayDescription',
  URL: 'url',
  IMAGE_ID: 'imageId',
  CONTACT_FIRSTNAME: 'contactFirstname',
  CONTACT_LASTNAME: 'contactLastname',
  CONTACT_EMAIL: 'contactEmail',
  CONTACT_PHONE: 'contactPhone',
  COMPLEMENTARY_INFORMATION: 'complementaryInformation',
  NETWORK_URLS: 'networkUrls'
}

export const OWNER_NETWORKS = {
  FACEBOOK: 'facebook',
  TWITTER: 'twitter',
  YOUTUBE: 'youtube',
  INSTAGRAM: 'instagram',
}

export const FORM_DETAILS = {
  REQUIRED_FIELDS: [
    OWNER_MODEL.NAME, OWNER_MODEL.CONTACT_EMAIL,
    OWNER_MODEL.IMAGE_ID, OWNER_MODEL.DISPLAY_DESCRIPTION
  ]
};
