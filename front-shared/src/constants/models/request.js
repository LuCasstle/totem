export const REQUEST_MODEL = {
  NAME: 'name',
  URL: 'url',
  STATUS: 'status',
  CONTACT_FIRSTNAME: 'contactFirstname',
  CONTACT_LASTNAME: 'contactLastname',
  CONTACT_EMAIL: 'contactEmail',
  CONTACT_PHONE: 'contactPhone',
  COMPLEMENTARY_INFORMATION: 'complementaryInformation',
  POST_ID: 'postId'
}

export const REQUEST_STATUSES = {
  ACCEPTED: 'accepted',
  REJECTED: 'rejected',
  PENDING: 'pending',
}

export const FORM_DETAILS = {
  REQUIRED_FIELDS: [REQUEST_MODEL.NAME, REQUEST_MODEL.CONTACT_EMAIL]
};
