export const POST_MODEL = {
  TITLE: 'title',
  IMAGE_ID: 'imageId',
  TAG_ID: 'tagId',
  DESCRIPTION: 'description',
  CONTENT: 'content',
};

export const FORM_DETAILS = {
  REQUIRED_FIELDS: [
    POST_MODEL.TITLE, POST_MODEL.IMAGE_ID,
    POST_MODEL.TAG_ID, POST_MODEL.DESCRIPTION
  ]
};
