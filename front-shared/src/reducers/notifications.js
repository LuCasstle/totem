import {
  PUSH_NOTIFICATION, NOTIFICATION_CONSUMED,
  SET_GLOBAL_LOADING, UNSET_GLOBAL_LOADING
} from '../constants/actions';
import { fromJS, List, Map } from 'immutable';
import uuid from 'uuid/v1';

const initialState = fromJS({
  notifications: List(),
  currentNotification: null,
  globalLoading: false
})

export const NOTIFICATION_ERROR = 'error';
export const NOTIFICATION_WARNING = 'warning';
export const NOTIFICATION_SUCCESS = 'success';

export default (state = initialState, action) => {
  switch (action.type) {
    case PUSH_NOTIFICATION:
      const newNotification = Map({
        uid: uuid(),
        type: action.notificationType,
        message: action.message
      });

      if (action.timeout) { newNotification.set('timeout', action.timeout) }

      if (!state.get('currentNotification')) {
        return state.set('currentNotification', newNotification);
      } else {
        return state.set('notifications', state.get('notifications').unshift(newNotification));
      }
    case NOTIFICATION_CONSUMED:
      let nextNotification = null;

      const notificationsSize = state.get('notifications').size;
      if (notificationsSize > 0) {
        nextNotification = state.getIn(['notifications', (notificationsSize - 1)]);
        state = state.set('notifications', state.get('notifications').pop())
      }

      return state.set('currentNotification', nextNotification);

    case SET_GLOBAL_LOADING:
      return state.set('globalLoading', true);
    case UNSET_GLOBAL_LOADING:
      return state.set('globalLoading', false);

    default:
      return state;
  }
}
