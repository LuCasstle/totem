import {
  NOTIFICATION_CONSUMED, PUSH_NOTIFICATION,
  SET_GLOBAL_LOADING, UNSET_GLOBAL_LOADING
} from '../constants/actions';

export const consumeNotification = () => ({
  type: NOTIFICATION_CONSUMED
})

export const pushNotification = notification => ({
  ...notification,
  type: PUSH_NOTIFICATION
})

export const unsetGlobalLoading = () => ({
  type: UNSET_GLOBAL_LOADING
})

export const setGlobalLoading = () => ({
  type: SET_GLOBAL_LOADING
})
