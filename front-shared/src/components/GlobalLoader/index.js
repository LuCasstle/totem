import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ProgressBar from 'react-toolbox/lib/progress_bar';
import styles from './styles.css';

const Wrapper = ({ children }) => (
  <div className={styles.Wrapper}>{children}</div>
)

class GlobalLoader extends Component {
  static PropTypes = {
    loading: PropTypes.bool.isRequired
  }

  render() {
    let content;

    if (this.props.loading) {
      content = <ProgressBar mode='indeterminate' multicolor/>
    }

    return (
      <Wrapper>{content}</Wrapper>
    );
  }
}

export default GlobalLoader;
