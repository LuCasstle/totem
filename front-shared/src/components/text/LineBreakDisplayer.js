import React from 'react';
import PropTypes from 'prop-types';
import { map, split } from 'lodash';

const LineBreakDisplayer = ({ text }) => (
  <div>
    {
      map(split(text, '\n'), (part, idx) => (
        <span key={idx}>
          {part}
          <br/>
        </span>
      ))
    }
  </div>
);

LineBreakDisplayer.PropTypes = {
  text: PropTypes.string.isRequired
}

export default LineBreakDisplayer;
