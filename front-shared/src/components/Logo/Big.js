import React from 'react';
import PropTypes from 'prop-types';

const logoPath = {
  yellow: '/assets/logo_flykke_626_210.png',
  grey: '/assets/logo_flykke_grey_482_81.png',
}

const BigLogo = ({ height = 105, color = 'yellow' }) => {
  return (
    <img
      src={logoPath[color]}
      alt={'logo'}
      height={height}
    />
  )
}

BigLogo.propTypes = {
  height: PropTypes.number.isRequired
};

export default BigLogo;
