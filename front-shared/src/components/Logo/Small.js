import React from 'react';
import PropTypes from 'prop-types';

const SmallLogo = ({ height = 25 }) => {
  return (
    <img
      src={'/logo_flykke_150_50.png'}
      alt={'logo'}
      height={height}
    />
  )
}

SmallLogo.propTypes = {
  height: PropTypes.number.isRequired
};

export default SmallLogo;
