import React from 'react';
import PropTypes from 'prop-types';
import { Link, Route } from 'react-router-dom';
import { active } from '../../styles/links.css';
import FontIcon from 'react-toolbox/lib/font_icon';

const NavLink = ({ label, to, icon, activeOnlyWhenExact, ...rest }) => {
  let { additonalClassName, ...others } = rest;

  return (
    <Route path={to} exact={activeOnlyWhenExact} children={({ match }) => (
      <Link {...others} className={additonalClassName + ' ' + (match ? active : '')} to={to}>
        {
          icon &&
          <FontIcon
            value={icon}
            style={{
              verticalAlign: 'middle',
              paddingBottom: 3,
              marginRight: 5
            }}
          />
        }
        {label}
      </Link>
    )}/>
  );
}

NavLink.propTypes = {
  label: PropTypes.any.isRequired,
  to: PropTypes.string.isRequired,
  activeOnlyWhenExact: PropTypes.bool
};

export default NavLink;
