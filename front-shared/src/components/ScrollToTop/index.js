import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import $ from 'jquery';

class ScrollToTop extends Component {
  static propTypes = {
    window: PropTypes.bool
  }

  componentDidUpdate(prevProps) {
    if (this.props.location !== prevProps.location) {
      if (this.props.window) {
        $(window).scrollTop(0)
      } else {
        $('#scrollable-container').scrollTop(0);
      }
    }
  }

  render() {
    return this.props.children
  }
}

export default withRouter(ScrollToTop);
