import React from 'react';
import theme from './styles.css';

export default (props) => {
  const {url} = props.contentState.getEntity(props.entityKey).getData();
  return (
    <a href={url} className={theme.link}>
      {props.children}
    </a>
  );
};
