import { CompositeDecorator } from 'draft-js';
import findLinkEntities from './Link/decorator';
import Link from './Link';

export default new CompositeDecorator([
  {
    strategy: findLinkEntities,
    component: Link,
  },
]);
