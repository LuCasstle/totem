import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { EditorState, RichUtils } from 'draft-js';

import theme from './styles.css';
import { IconButton } from 'react-toolbox/lib/button';
import Input from 'react-toolbox/lib/input';

export const INPUT_NAME = 'link';
let initialState = {
  urlValue: ''
};

export default class LinkControls extends Component {
  static propTypes = {
    editorState: PropTypes.object.isRequired,
    changeEditorState: PropTypes.func.isRequired,
    focusEditor: PropTypes.func.isRequired,
    toggleInput: PropTypes.func.isRequired,
    showInput: PropTypes.string,
  }

  state = initialState

  componentWillReceiveProps(nextProps) {
    if (nextProps.showInput !== INPUT_NAME) {
      this.setState(initialState);
    }
  }

  onURLChange = (urlValue) => this.setState({urlValue})

  onURLInputKeyDown = (e) => {
    if (e.which === 13) {
      this.confirmLink(e);
    }
  }

  promptForLink = () => {
    const {editorState} = this.props;
    const selection = editorState.getSelection();
    if (!selection.isCollapsed()) {
      this.props.toggleInput(INPUT_NAME);
      const contentState = editorState.getCurrentContent();
      const startKey = editorState.getSelection().getStartKey();
      const startOffset = editorState.getSelection().getStartOffset();
      const blockWithLinkAtBeginning = contentState.getBlockForKey(startKey);
      const linkKey = blockWithLinkAtBeginning.getEntityAt(startOffset);

      let url = '';
      if (linkKey) {
        const linkInstance = contentState.getEntity(linkKey);
        url = linkInstance.getData().url;
      }

      this.setState({
        urlValue: url,
      }, () => {
        setTimeout(() => this.urlContainer.querySelector('input').focus(), 0);
      });
    }
  }

  confirmLink = () => {
    const {changeEditorState, editorState, focusEditor} = this.props;
    const {urlValue} = this.state;
    const contentState = editorState.getCurrentContent();
    const contentStateWithEntity = contentState.createEntity(
      'LINK',
      'MUTABLE',
      {url: urlValue}
    );
    const entityKey = contentStateWithEntity.getLastCreatedEntityKey();
    const newEditorState = EditorState.set(editorState, { currentContent: contentStateWithEntity });

    changeEditorState(RichUtils.toggleLink(
      newEditorState,
      newEditorState.getSelection(),
      entityKey
    ));

    this.props.toggleInput(null, focusEditor);
  }

  removeLink = () => {
    const {editorState, changeEditorState} = this.props;
    const selection = editorState.getSelection();
    if (!selection.isCollapsed()) {
      changeEditorState(RichUtils.toggleLink(editorState, selection, null));
    }
  }

  isInputActive = () => this.props.showInput === INPUT_NAME

  render() {
    let urlInput;
    if (this.isInputActive()) {
      urlInput =
        <div ref={node => this.urlContainer = node}>
          <Input
            type='text'
            name='url'
            theme={theme}
            value={this.state.urlValue}
            onChange={this.onURLChange}
            onKeyPress={this.onURLInputKeyDown}
          />
          <IconButton icon='add' floating accent mini onClick={this.confirmMedia}/>
        </div>;
    }

    return (
      <div className={(this.isInputActive() ? theme.borderedContainer : theme.container)}>
        <div>
          <IconButton
            icon='link' raised mini primary={this.props.showInput === INPUT_NAME}
            style={{marginRight: 10}}
            onClick={this.promptForLink}
          />
          <IconButton
            icon='link' raised mini accent
            style={{marginRight: 10}}
            onClick={this.removeLink}
          />
        </div>
        {urlInput}
      </div>
    );

  }
}
