import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { RichUtils } from 'draft-js';

import theme from './styles.css';
import InlineStyleControls from './InlineStyleControls/index.js';
import BlockStyleControls from './BlockStyleControls/index.js';
import MediaControls, { INPUT_NAME as MEDIA_INPUT_NAME } from './Media';
import LinkControls, { INPUT_NAME as LINK_INPUT_NAME } from './Link';
import $ from 'jquery';

const POSITIONS = {
  FIXED: 'fixed',
  RELATIVE: 'relative'
}

export default class ToolBox extends Component {
  static propTypes = {
    editorState: PropTypes.object.isRequired,
    changeEditorState: PropTypes.func.isRequired,
    focusEditor: PropTypes.func.isRequired
  }

  state = {
    position: POSITIONS.RELATIVE,
    showInput: null
  }

  toggleInput = (inputName, cb) => {
    this.setState({ showInput: inputName }, () => {
      if (cb) setTimeout(() => cb, 0);
    });
  }

  componentDidMount() {
    // this.scrollableContainer = $('#scrollable-container');
    // this.scrollableContainer.bind('scroll', this.onScroll);
    //
    // this.lastScrollTop = this.scrollableContainer.scrollTop();
    // this.onScroll();
  }

  componentWillUnmount() {
    // this.scrollableContainer.unbind('scroll', this.onScroll);
  }

  onScroll = (e) => {
    const { position } = this.state;
    const newScrollTop = this.scrollableContainer.scrollTop();

    let shouldBeFixed, shouldbeUnfixed = false;
    if (position === POSITIONS.RELATIVE) {
      const viewportOffset = this.container.getBoundingClientRect();
      let relativeContainerTop = viewportOffset.top;
      shouldBeFixed = relativeContainerTop <= 64;
    } else if (position === POSITIONS.FIXED) {
      shouldbeUnfixed = (newScrollTop) < this.breakingScroll;
    }

    if (newScrollTop > this.lastScrollTop && shouldBeFixed && position === POSITIONS.RELATIVE) {
      this.scrollableContainer.scrollTop(newScrollTop - 157);
      this.breakingScroll = newScrollTop - 157;
      this.setState({
        position: POSITIONS.FIXED,
        width: (this.scrollableContainer.width() + 60),
        left: ($('#left-nav').width() + 2)
      });
      this.timeoutTime = 0;
    } else if (newScrollTop < this.lastScrollTop && shouldbeUnfixed && position === POSITIONS.FIXED) {
      this.scrollableContainer.scrollTop(newScrollTop + 157);
      this.setState({ position: POSITIONS.RELATIVE });
    }

    this.lastScrollTop = newScrollTop;
  }

  toggleBlockType = (blockType) => {
    const { editorState, changeEditorState } = this.props;

    changeEditorState(
      RichUtils.toggleBlockType(
        editorState,
        blockType
      )
    );
  }

  toggleInlineStyle = (inlineStyle) => {
    const { editorState, changeEditorState } = this.props;

    changeEditorState(
      RichUtils.toggleInlineStyle(
        editorState,
        inlineStyle
      )
    );
  }

  render() {
    let style = {
      position: this.state.position
    }

    if (this.state.position === POSITIONS.FIXED) {
      style = {
        ...style,
        top: 64,
        width: this.state.width,
        left: this.state.left,
        padding: '15px 10px 10px 10px'
      }
    }

    const { editorState, changeEditorState, focusEditor } = this.props;

    return (
      <div className={theme.ToolBox} ref={(node) => this.container = node} style={style}>
        <BlockStyleControls
          editorState={editorState}
          onToggle={this.toggleBlockType}
        />
        <InlineStyleControls
          editorState={editorState}
          toggleInlineStyle={this.toggleInlineStyle}
          changeEditorState={changeEditorState}
        />
        <div className={theme.MediaLinkControlsContainer}>
          <LinkControls
            editorState={editorState}
            changeEditorState={changeEditorState}
            focusEditor={focusEditor}
            toggleInput={this.toggleInput}
            showInput={this.state.showInput}
          />
          <MediaControls
            editorState={editorState}
            changeEditorState={changeEditorState}
            focusEditor={focusEditor}
            toggleInput={this.toggleInput}
            showInput={this.state.showInput}
          />
        </div>
      </div>
    );
  }
}
