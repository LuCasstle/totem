import React, { Component } from 'react';
import { EditorState, AtomicBlockUtils } from 'draft-js';
import PropTypes from 'prop-types';

import MEDIA_TYPES from './types';
import theme from './styles.css';
import { IconButton } from 'react-toolbox/lib/button';
import Input from 'react-toolbox/lib/input';

export const INPUT_NAME = 'media';

let initialState = {
  url: '',
  urlType: ''
};

export default class MediaControls extends Component {
  static propTypes = {
    editorState: PropTypes.object.isRequired,
    changeEditorState: PropTypes.func.isRequired,
    focusEditor: PropTypes.func.isRequired,
    toggleInput: PropTypes.func.isRequired,
    showInput: PropTypes.string,
  }

  state = initialState

  componentWillReceiveProps(nextProps) {
    if (nextProps.showInput !== INPUT_NAME) {
      this.setState(initialState);
    }
  }

  onURLChange = (urlValue) => this.setState({urlValue})

  onURLInputKeyDown = (e) => {
    if (e.which === 13) {
      this.confirmMedia(e);
    }
  }

  promptForMedia = (type) => {
    this.props.toggleInput(INPUT_NAME);
    this.setState({
      urlValue: '',
      urlType: type,
    }, () => {
      setTimeout(() => this.urlContainer.querySelector('input').focus(), 0);
    });
  }

  addAudio = () => {
    this.promptForMedia(MEDIA_TYPES.AUDIO);
  }

  addImage = () => {
    this.promptForMedia(MEDIA_TYPES.IMAGE);
  }

  addVideo = () => {
    this.promptForMedia(MEDIA_TYPES.VIDEO);
  }

  confirmMedia = (e) => {
    e.preventDefault();
    const {urlType, urlValue} = this.state;
    const {editorState, changeEditorState, focusEditor} = this.props;
    const contentState = editorState.getCurrentContent();
    const contentStateWithEntity = contentState.createEntity(
      urlType,
      'IMMUTABLE',
      {src: urlValue}
    );
    const entityKey = contentStateWithEntity.getLastCreatedEntityKey();
    const newEditorState = EditorState.set(
      editorState,
      {currentContent: contentStateWithEntity}
    );

    changeEditorState(AtomicBlockUtils.insertAtomicBlock(
      newEditorState,
      entityKey,
      ' '
    ));

    this.props.toggleInput(null, focusEditor);
  }

  isInputActive = () => this.props.showInput === INPUT_NAME

  render() {
    let urlInput;
    if (this.isInputActive()) {
      urlInput =
        <div ref={node => this.urlContainer = node}>
          <Input
            type='text'
            name='url'
            theme={theme}
            value={this.state.urlValue}
            onChange={this.onURLChange}
            onKeyPress={this.onURLInputKeyDown}
          />
          <IconButton icon='add' floating accent mini onClick={this.confirmMedia}/>
        </div>;
    }

    const { urlType } = this.state;

    return (
      <div className={(this.isInputActive() ? theme.borderedContainer : theme.container)}>
        <div>
          <IconButton
            icon='music_note' raised floating mini
            style={{marginRight: 10}}
            onClick={this.addAudio}
            primary={urlType === MEDIA_TYPES.AUDIO}
          />
          <IconButton
            icon='image' raised mini floating
            style={{marginRight: 10}}
            onClick={this.addImage}
            primary={urlType === MEDIA_TYPES.IMAGE}
          />
          <IconButton
            icon='movie' raised mini
            style={{marginRight: 10}}
            onClick={this.addVideo}
            primary={urlType === MEDIA_TYPES.VIDEO}
          />
        </div>
        {urlInput}
      </div>
    );

  }
}
