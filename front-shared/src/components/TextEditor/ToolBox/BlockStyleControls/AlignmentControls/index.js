import React from 'react';
import BlockStyleButton from '../../buttons/BlockStyleButton';
import { ALIGNMENT_BLOCK_TYPES } from '../blockTypes';
import theme from '../styles.css';
import { includes } from 'lodash';

const isActive = (blockType, style) => includes(blockType, style)

export default (props) => {
  return (
    <div className={theme.AlignmentControls}>
      {ALIGNMENT_BLOCK_TYPES.map((type) =>
        <BlockStyleButton
          key={type.icon}
          active={isActive(props.blockType, type.style)}
          primary={isActive(props.blockType, type.style)}
          icon={type.icon}
          onToggle={props.onToggle}
          style={type.style}
        />
      )}
    </div>
  )
}
