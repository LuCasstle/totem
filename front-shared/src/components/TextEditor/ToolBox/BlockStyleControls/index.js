import React, { Component } from 'react';
import { Map } from 'immutable';
import theme from '../styles.css';
import editorTheme from '../../styles.css';
import BlockStyleButton from '../buttons/BlockStyleButton';
import { includes } from 'lodash';
import AlignmentControls from './AlignmentControls';
import {
  BLOCK_TYPES, ALIGNMENT_BLOCK_TYPES, BLOCK_TYPES_ASSOCIATIONS,
  UNSTYLED, TYPE_TO_ELEMENT, UL, OL, ALIGNED_LEFT, ALIGNED_RIGHT, ALIGNED_CENTER
} from './blockTypes';
import { keys, forEach, lowerCase } from 'lodash';

const buildBlockType = (currentBlockType, selectedBlockType) => {
  if (BLOCK_TYPES_ASSOCIATIONS[currentBlockType] && BLOCK_TYPES_ASSOCIATIONS[currentBlockType][selectedBlockType]) {
    return BLOCK_TYPES_ASSOCIATIONS[currentBlockType][selectedBlockType];
  } else if (includes(currentBlockType, 'aligned') && includes(selectedBlockType, 'aligned')) {
    let split = currentBlockType.split('_');
    if (split.length > 1) {
      return BLOCK_TYPES_ASSOCIATIONS[split[0]][selectedBlockType];
    } else {
      return selectedBlockType;
    }
  } else if (includes(currentBlockType, 'aligned') && !includes(selectedBlockType, 'aligned')) {
    let split = currentBlockType.split('_');
    if (split.length > 1) {
      return BLOCK_TYPES_ASSOCIATIONS[selectedBlockType][split[1]];
    } else if (split.length == 1) {
      return BLOCK_TYPES_ASSOCIATIONS[selectedBlockType][split[0]];
    } else {
      return selectedBlockType;
    }
  }

  return selectedBlockType;
}

const isActive = (blockType, type) => {
  if (blockType === UL || blockType === OL) {
    return blockType === type;
  }
  return includes(blockType, type)
}

class BlockStyleControls extends Component {
  onToggle = (newBlockType) => {
    const {editorState} = this.props;
    const selection = editorState.getSelection();
    const currentBlockType = editorState
      .getCurrentContent()
      .getBlockForKey(selection.getStartKey())
      .getType();

    this.props.onToggle(buildBlockType(currentBlockType, newBlockType));
  }

  render() {
    const {editorState} = this.props;
    const selection = editorState.getSelection();
    const blockType = editorState
      .getCurrentContent()
      .getBlockForKey(selection.getStartKey())
      .getType();

    return (
      <div className={theme.RichEditorControls}>
        {BLOCK_TYPES.map((type) =>
          <BlockStyleButton
            key={type.label}
            active={isActive(blockType, type.style)}
            label={type.label}
            onToggle={this.onToggle}
            style={type.style}
          />
        )}

        <AlignmentControls
          onToggle={this.onToggle}
          blockType={blockType}
        />
      </div>
    );
  }
};

export default BlockStyleControls;

export const getBlockStyle = (block) => {
  const blockType = block.getType();

  if (includes(blockType, 'blockquote') && includes(blockType, 'aligned')) {
    return editorTheme.RichEditorBlockquote + ' ' + editorTheme[blockType.split('_')[1]];
  }

  if (includes(blockType, 'aligned')) {
    let split = blockType.split('_');
    if (split.length > 1) {
      return editorTheme[split[1]]
    } else {
      return editorTheme[split[0]]
    }
  }

  switch (blockType) {
    case 'blockquote': return editorTheme.RichEditorBlockquote;
    default: return null;
  }
}

const blockRenderMapping = {};
forEach(BLOCK_TYPES_ASSOCIATIONS, (associations, associationsName) => {
  forEach(associations, (val, key) => {
    if (!includes(key, 'aligned')) { return; }
    let textType = val.split('_')[0];
    blockRenderMapping[val] = {
      element: TYPE_TO_ELEMENT[textType]
    }
  })
});

forEach([ALIGNED_LEFT, ALIGNED_RIGHT, ALIGNED_CENTER], (val) => {
  blockRenderMapping[val] = TYPE_TO_ELEMENT[val]
})

export const blockRenderMap = Map(blockRenderMapping)
