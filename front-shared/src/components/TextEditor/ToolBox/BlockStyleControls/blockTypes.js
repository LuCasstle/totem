export const ALIGNED_LEFT = 'aligned-left';
export const ALIGNED_CENTER = 'aligned-center';
export const ALIGNED_RIGHT = 'aligned-right';
export const H1 = 'header-one';
export const H2 = 'header-two';
export const H3 = 'header-three';
export const H4 = 'header-four';
export const H5 = 'header-five';
export const H6 = 'header-six';
export const BLOCKQUOTE = 'blockquote';
export const UNSTYLED = 'unstyled';
export const UL = 'unordered-list-item';
export const OL = 'ordered-list-item';

export const BLOCK_TYPES = [
  {label: 'H1', style: H1},
  {label: 'H2', style: H2},
  {label: 'H3', style: H3},
  {label: 'H4', style: H4},
  {label: 'H5', style: H5},
  {label: 'H6', style: H6},
  {label: 'Blockquote', style: BLOCKQUOTE},
  {label: 'UL', style: UL},
  {label: 'OL', style: OL},
  {label: 'Code Block', style: 'code-block'},
];

export const ALIGNMENT_BLOCK_TYPES = [
  {icon: 'format_align_left', style: ALIGNED_LEFT},
  {icon: 'format_align_center', style: ALIGNED_CENTER},
  {icon: 'format_align_right', style: ALIGNED_RIGHT}
]

const TITLE_ASSOCIATIONS = (hType) => ({
  [ALIGNED_LEFT]: (hType + '_' + ALIGNED_LEFT),
  [ALIGNED_CENTER]: (hType + '_' + ALIGNED_CENTER),
  [ALIGNED_RIGHT]: (hType + '_' + ALIGNED_RIGHT)
})

const ALIGN_ASSOCIATIONS = (alignedType) => ({
  [H1]: (H1 + '_' + alignedType),
  [H2]: (H2 + '_' + alignedType),
  [H3]: (H3 + '_' + alignedType),
  [H4]: (H4 + '_' + alignedType),
  [H5]: (H5 + '_' + alignedType),
  [H6]: (H6 + '_' + alignedType)
})

const BLOCKQUOTE_ASSOCIATIONS = {
  [ALIGNED_LEFT]: (BLOCKQUOTE + '_' + ALIGNED_LEFT),
  [ALIGNED_CENTER]: (BLOCKQUOTE + '_' + ALIGNED_CENTER),
  [ALIGNED_RIGHT]: (BLOCKQUOTE + '_' + ALIGNED_RIGHT),
};

export const BLOCK_TYPES_ASSOCIATIONS = {
  [H1]: TITLE_ASSOCIATIONS(H1),
  [H2]: TITLE_ASSOCIATIONS(H2),
  [H3]: TITLE_ASSOCIATIONS(H3),
  [H4]: TITLE_ASSOCIATIONS(H4),
  [H5]: TITLE_ASSOCIATIONS(H5),
  [H6]: TITLE_ASSOCIATIONS(H6),
  [BLOCKQUOTE]: BLOCKQUOTE_ASSOCIATIONS,

  [ALIGNED_LEFT]: ALIGN_ASSOCIATIONS(ALIGNED_LEFT),
  [ALIGNED_CENTER]: ALIGN_ASSOCIATIONS(ALIGNED_CENTER),
  [ALIGNED_RIGHT]: ALIGN_ASSOCIATIONS(ALIGNED_RIGHT),
}

export const TYPE_TO_ELEMENT = {
  [H1]: 'h1',
  [H2]: 'h2',
  [H3]: 'h3',
  [H4]: 'h4',
  [H5]: 'h5',
  [H6]: 'h6',
  [BLOCKQUOTE]: 'blockquote',
  [ALIGNED_LEFT]: 'p',
  [ALIGNED_CENTER]: 'p',
  [ALIGNED_RIGHT]: 'p',
}
