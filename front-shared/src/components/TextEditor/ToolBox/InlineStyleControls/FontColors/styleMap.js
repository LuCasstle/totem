// This object provides the styling information for our custom color
// styles.
export default {
  ownRed: {
    color: 'rgba(255, 0, 0, 1.0)',
  },
  ownOrange: {
    color: 'rgba(255, 127, 0, 1.0)',
  },
  ownYellow: {
    color: 'rgba(180, 180, 0, 1.0)',
  },
  ownGreen: {
    color: 'rgba(0, 180, 0, 1.0)',
  },
  ownBlue: {
    color: 'rgba(0, 0, 255, 1.0)',
  },
  ownIndigo: {
    color: 'rgba(75, 0, 130, 1.0)',
  },
  ownViolet: {
    color: 'rgba(127, 0, 255, 1.0)',
  },
};
