import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { EditorState, RichUtils, Modifier } from 'draft-js';
import COLORS from './colors';
import colorStyleMap from './styleMap';
import ColorButton from '../../buttons/ColorButton';

export default class FontColors extends Component {
  static propTypes = {
    editorState: PropTypes.object.isRequired,
    changeEditorState: PropTypes.func.isRequired,
  }

  toggleColor = (toggledColor) => {
    const {editorState} = this.props;
    const selection = editorState.getSelection();

    // Let's just allow one color at a time. Turn off all active colors.
    const nextContentState = Object.keys(colorStyleMap)
      .reduce((contentState, color) => {
        return Modifier.removeInlineStyle(contentState, selection, color)
      }, editorState.getCurrentContent());

    let nextEditorState = EditorState.push(
      editorState,
      nextContentState,
      'change-inline-style'
    );

    const currentStyle = editorState.getCurrentInlineStyle();

    // Unset style override for current color.
    if (selection.isCollapsed()) {
      nextEditorState = currentStyle.reduce((state, color) => {
        return RichUtils.toggleInlineStyle(state, color);
      }, nextEditorState);
    }

    // If the color is being toggled on, apply it.
    if (!currentStyle.has(toggledColor)) {
      nextEditorState = RichUtils.toggleInlineStyle(
        nextEditorState,
        toggledColor
      );
    }

    this.props.changeEditorState(nextEditorState);
  }

  render() {
    const { editorState } = this.props;
    const currentStyle = editorState.getCurrentInlineStyle();

    return (
      <div>
        {COLORS.map(type =>
          <ColorButton
            active={currentStyle.has(type.style)}
            label={type.label}
            onToggle={this.toggleColor}
            style={type.style}
            key={type.label}
          />
        )}
      </div>
    );
  }
}
