export default [
  {label: 'Red', style: 'ownRed'},
  {label: 'Orange', style: 'ownOrange'},
  {label: 'Yellow', style: 'ownYellow'},
  {label: 'Green', style: 'ownGreen'},
  {label: 'Blue', style: 'ownBlue'},
  {label: 'Indigo', style: 'ownIndigo'},
  {label: 'Violet', style: 'ownViolet'},
];
