import React from 'react';
import theme from '../styles.css';
import FontStyleControls from './FontStyle';
import FontColorControls from './FontColors';

const InlineStyleControls = (props) => {
  return (
    <div className={theme.RichEditorControls}>
      <FontStyleControls
        editorState={props.editorState}
        onToggle={props.toggleInlineStyle}
      />
      <FontColorControls
        editorState={props.editorState}
        changeEditorState={props.changeEditorState}
      />
    </div>
  )
};

export default InlineStyleControls;
