import React, { Component } from 'react';
import StyleButton from './StyleButton';
import IconButton from './IconButton';
import theme from './styles.css';

class BlockStyleButton extends Component {
  onToggle = (e) => {
    e.preventDefault();
    this.props.onToggle(this.props.style);
  };

  render() {
    const { label, active, icon, accent, primary } = this.props;

    if (label) {
      return (
        <StyleButton
          onToggle={this.onToggle}
          label={label}
          className={active ? theme.ActiveFontStyleButton : null}
        />
      );
    } else if (icon) {
      return (
        <IconButton
          accent={accent}
          primary={primary}
          icon={icon}
          onToggle={this.onToggle}
        />
      )
    }
  }
}

export default BlockStyleButton;
