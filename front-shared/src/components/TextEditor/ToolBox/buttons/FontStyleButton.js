import React, { Component } from 'react';
import StyleButton from './StyleButton';
import theme from './styles.css';

class FontStyleButton extends Component {

  onToggle = (e) => {
    e.preventDefault();
    this.props.onToggle(this.props.style);
  }

  render() {
    const { label, active } = this.props;
    return (
      <StyleButton
        onToggle={this.onToggle}
        className={active ? theme.ActiveFontStyleButton : null}
        label={label}
      />
    )
  }
}

export default FontStyleButton;
