import React, { Component } from 'react';
import StyleButton from './StyleButton';
import colorStyleMap from '../InlineStyleControls/FontColors/styleMap';

class ColorButton extends Component {
  onToggle = (e) => {
    e.preventDefault();
    this.props.onToggle(this.props.style);
  };

  render() {
    const { style, active } = this.props;

    return (
      <StyleButton
        onToggle={this.onToggle}
        style={active ? colorStyleMap[style] : null}
        label={this.props.label}
      />
    );
  }
}

export default ColorButton;
