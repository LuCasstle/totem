import React from 'react';
import theme from './styles.css';

const StyleButton = ({ label, className, style, onToggle }) => (
  <span className={className || theme.StyleButton} style={style} onMouseDown={onToggle}>
    {label}
  </span>
);

export default StyleButton;
