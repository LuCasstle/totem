import React from 'react';
import { IconButton } from 'react-toolbox/lib/button';

export default ({ icon, active, primary, accent, flat, onToggle }) => (
  <IconButton icon={icon} accent={accent} primary={primary} onMouseUp={onToggle} />
);
