import React from 'react';
import theme from './styles.css';

const Image = (props) => {
  return <img src={props.src} className={theme.media} />;
};

export default Image;
