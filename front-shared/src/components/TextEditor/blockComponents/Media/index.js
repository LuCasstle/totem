import React from 'react';
import MEDIA_TYPES from '../../ToolBox/Media/types';
import Video from './video';
import Audio from './audio';
import Image from './image';

const Media = (props) => {
  const entity = props.contentState.getEntity(
    props.block.getEntityAt(0)
  );
  const {src} = entity.getData();
  const type = entity.getType();

  let media;
  if (type === MEDIA_TYPES.AUDIO) {
    media = <Audio src={src} />;
  } else if (type === MEDIA_TYPES.IMAGE) {
    media = <Image src={src} />;
  } else if (type === MEDIA_TYPES.VIDEO) {
    media = <Video src={src} />;
  }

  return media;
};

export default Media;

export const mediaBlockRenderer = (block) => {
  if (block.getType() === 'atomic') {
    return {
      component: Media,
      editable: false,
    };
  }

  return null;
}
