import React from 'react';
import Media from './index.js';
import theme from './styles.css';

const YOUTUBEMATCH_URL = /^(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/

const Video = ({ src }) => {
  const id = src && src.match(YOUTUBEMATCH_URL)[1];
  if (id) {
    return (
      <iframe id="ytplayer" className={theme.video} type="text/html"
        src={`https://www.youtube.com/embed/${id}?origin=http://flykke.com`}
        frameBorder="0" />
    );
  }

  return <video controls src={props.src} className={theme.media} />;
};

export default Video;
