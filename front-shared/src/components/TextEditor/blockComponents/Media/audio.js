import React from 'react';
import theme from './styles.css';

const Audio = (props) => {
  return <audio controls src={props.src} className={theme.media} />;
};

export default Audio;
