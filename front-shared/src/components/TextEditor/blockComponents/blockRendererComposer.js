export default (blockRendererFns) => {
  return (block) => {
    let res = null;
    let ind = 0;
    while (!res && ind < blockRendererFns.length) {
      res = blockRendererFns[ind](block);
      ind ++;
    }

    return res;
  }
}
