import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Editor, EditorState, RichUtils,
  Modifier, DefaultDraftBlockRenderMap
} from 'draft-js';

import theme from './styles.css';
import ToolBox from './ToolBox';
import colorStyleMap from './ToolBox/InlineStyleControls/FontColors/styleMap';
import fontStyleMap from './ToolBox/InlineStyleControls/FontStyle/styleMap';
import { getBlockStyle, blockRenderMap } from './ToolBox/BlockStyleControls/index.js';
import blockRendererComposer from './blockComponents/blockRendererComposer';
import { mediaBlockRenderer } from './blockComponents/Media';

// Custom overrides for "code" style.
const styleMap = {
  ...colorStyleMap,
  ...fontStyleMap
};

const composedBlockRenderer = blockRendererComposer([mediaBlockRenderer])
const extendedBlockRenderMap = DefaultDraftBlockRenderMap.merge(blockRenderMap);

class TextEditor extends React.Component {
  static propTypes = {
    editorState: PropTypes.object,
    updateEditorState: PropTypes.func,
    displayOnly: PropTypes.bool,
  }

  focus = () => { this.editor.focus() }
  onChange = (editorState) => this.props.updateEditorState(editorState)

  handleKeyCommand = (command) => {
    const {editorState} = this.props;
    const newState = RichUtils.handleKeyCommand(editorState, command);
    if (newState) {
      this.onChange(newState);
      return true;
    }
    return false;
  }

  onTab = (e) => {
    const maxDepth = 4;
    this.onChange(RichUtils.onTab(e, this.props.editorState, maxDepth));
  }

  render() {
    if (!this.props.editorState) {
      return <div></div>;
    }

    const {editorState, displayOnly} = this.props;

    // If the user changes block type before entering any text, we can
    // either style the placeholder or hide it. Let's just hide it now.
    let className = theme.RichEditorEditor;
    const contentState = editorState.getCurrentContent();
    if (!contentState.hasText()) {
      if (contentState.getBlockMap().first().getType() !== 'unstyled') {
        className += ` ${theme.RichEditorHidePlaceholder}`
      }
    }

    return (
      <div className={theme.RichEditorRoot} style={{ padding: (displayOnly ? 0 : 15), paddingTop: 0 }}>
        { !displayOnly &&
          <ToolBox
            editorState={editorState}
            changeEditorState={this.onChange}
            focusEditor={this.focus}
          />
        }
        <div className={className} onClick={this.focus}>
          <Editor
            readOnly={this.props.displayOnly}
            blockStyleFn={getBlockStyle}
            blockRendererFn={composedBlockRenderer}
            blockRenderMap={extendedBlockRenderMap}
            customStyleMap={styleMap}
            editorState={editorState}
            handleKeyCommand={this.handleKeyCommand}
            readOnly={displayOnly}
            onChange={this.onChange}
            onTab={this.onTab}
            placeholder="Tell a story..."
            ref={node => this.editor = node}
            spellCheck={true}
          />
        </div>
      </div>
    );
  }
}

export default TextEditor;
