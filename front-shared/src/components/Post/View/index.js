import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Redirect } from 'react-router-dom';
import moment from 'moment';
import styles from './styles.css';
import { Card, CardMedia } from 'react-toolbox/lib/card';
import { w1000 } from 'shared/src/helpers/cloudinary';
import Button from 'react-toolbox/lib/button';
import TextEditor from 'shared/src/components/TextEditor';
import EmbeddedOwner from '../../Owner/Embedded';
import SubHeader from '../../SubHeader';

class Post extends Component {
  static propTypes = {
    post: PropTypes.object,
    image: PropTypes.object,
    tag: PropTypes.object,
    owner: PropTypes.object,
    thumbnail: PropTypes.object,

    onShareClick: PropTypes.func
  }

  render() {
    const {
      post, image, tag, thumbnail, owner,
      onShareClick
    } = this.props;

    if (!post) {
      return <div></div>
    }

    return (
      <div>
        <SubHeader>
          <h2 className={styles.SubHeaderTitle}>{post.get('title')}</h2>
          <h4>Créé { moment(post.get('createdAt')).fromNow() } &middot; { tag.get('name') }</h4>
        </SubHeader>

        <div className={styles.PostHeader}>
          <div className={styles.PostImage}>
            <Card>
              <CardMedia
                aspectRatio="wide"
                image={w1000(image.getIn(['srcInfo', 'url']))}
              />
            </Card>
          </div>
          <div className={styles.PostDonationsSummary}>
            <h1 className={styles.PostDonationsSummaryTitle}>{post.get('donations') || 0}</h1>
            <p style={{ marginBottom: 20, textAlign: 'center' }}>personnes ont déjà soutenu ce projet en faisant un don gratuit</p>
            <p className={styles.PostDonationsSummaryConversion}>soit {(post.get('donations') || 0) * 0.40}&euro; de récoltés</p>
            <div><Button disabled primary theme={{ button: styles.PostDonationsSummaryButton }}>Soutenez ce projet</Button></div>
            <div>
              <Button
                disabled={!onShareClick}
                theme={{ button: styles.PostDonationsSummaryButton }}
                onClick={onShareClick}>
                Partager
              </Button>
            </div>
          </div>
        </div>

        <div className={styles.TextEditorWrapper}>
          <TextEditor
            displayOnly={true}
            editorState={post.get('editorState')}
            updateEditorState={() => {}}
          />

          {
            owner &&
            <div style={{ marginTop: 50 }}>
              <EmbeddedOwner
                owner={owner}
                thumbnail={thumbnail}
              />
            </div>
          }
        </div>
      </div>
    );
  }
}

export default Post;
