import React, { Component } from 'react';
import $ from 'jquery';

class ScrollToTopOnMount extends Component {
  componentWillMount = () => $('#scrollable-container').scrollTop(0)

  render() {
    return <div></div>
  }
}

export default ScrollToTopOnMount;
