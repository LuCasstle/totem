import React, { Component } from 'react';
import PropTypes from 'prop-types';
import $ from 'jquery';

class DetectScrollWrapper extends Component {
  static propTypes = {
    action: PropTypes.func.isRequired,
    scrollDownThreshold: PropTypes.number,
    useRootAsReferential: PropTypes.bool,
  }

  static defaultProps = {
    scrollDownThreshold: 90,
    useRootAsReferential: false
  }

  componentDidMount() {
    this.previousScrolled = $(window).scrollTop();
    window.addEventListener("scroll", this.handleScroll);
  }

  componentWillUnmount() {
    window.removeEventListener("scroll", this.handleScroll);
  }

  timeout = null

  handleScroll = () => {
    const root = $('#root'), w = $(window);
    const scrolled = w.scrollTop();

    if (scrolled < this.previousScrolled) {
      this.previousScrolled = scrolled;
      return;
    }

    this.previousScrolled = scrolled;

    if (this.timeout) clearTimeout(this.timeout);

    this.timeout = setTimeout(() => {
      if (this.props.useRootAsReferential) {

        const viewPortHeight = w.innerHeight(), contentHeight = root.innerHeight();
        const percentageScrolled = 100  * (scrolled + innerHeight) / contentHeight

        if (percentageScrolled > this.props.scrollDownThreshold) {
          this.props.action();
        }

        this.timeout = null;
      }
    }, 200);
  }

  render() {
    return <div ref={ node => this.container = node }>{this.props.children}</div>
  }
}

export default DetectScrollWrapper;
