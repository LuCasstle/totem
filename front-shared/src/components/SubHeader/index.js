import React from 'react';
import PropTypes from 'prop-types';
import classes from './styles.css'

const classesMapper = {
  centered: {
    SubHeader: classes.CenteredSubHeader,
    SubHeaderSub: classes.CenteredSubHeaderSub,
  },
  normal: {
    SubHeader: classes.NormalSubHeader,
    SubHeaderSub: classes.NormalSubHeaderSub,
  },
};

const SubHeader = ({ type = 'centered', children, style={} }) => (
  <div className={classesMapper[type].SubHeader}>
    <div className={classesMapper[type].SubHeaderSub}>
      { children }
    </div>
  </div>
);

SubHeader.propTypes = {
  type: PropTypes.string,
  children: PropTypes.any.isRequired,
  style: PropTypes.object
};

export default SubHeader;
