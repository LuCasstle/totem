import React from 'react';
import { Route } from 'react-router';

export default (route) => {
  return (
    <Route 
      path={route.path}
      exact={route.exact}
      render={route.render || (props => (
        // pass the sub-routes down to keep nesting
        <route.component {...props} routes={route.routes} />
      ))}
    />
  );
}
