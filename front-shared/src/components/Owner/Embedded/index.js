import React from 'react';
import PropTypes from 'prop-types';
import { thumbnail as cloudinaryThumbnail } from 'shared/src/helpers/cloudinary';
import Avatar from 'react-toolbox/lib/avatar';
import classes from './styles.css';
import { OWNER_MODEL, OWNER_NETWORKS } from 'shared/src/constants/models/owner';
import { map } from 'lodash';
import { URLS as NETWORK_IMG_URLS } from 'shared/src/constants/socialicons';

const EmbeddedOwner = ({ owner, thumbnail }) => {
  return (
    <div className={classes.Wrapper}>
      <div>
        <Avatar
          className={classes.Avatar}
          title={owner.get(OWNER_MODEL.NAME)}>
          <img src={cloudinaryThumbnail(thumbnail.getIn(['srcInfo', 'url']))} />
        </Avatar>
      </div>

      <div style={{ width: 'calc(100% - 65px)' }}>
        <h4 className={classes.title}>
          { owner.get(OWNER_MODEL.NAME) }
        </h4>

        <p className={classes.description}>{owner.get(OWNER_MODEL.DISPLAY_DESCRIPTION)}</p>

        {
          owner.get(OWNER_MODEL.NETWORK_URLS).size > 0 &&
          <div>
            {
              map(owner.get(OWNER_MODEL.NETWORK_URLS).toJS(), (networkUrl, network) => (
                <a
                  href={`http://${networkUrl}`}
                  target='_blank'
                  key={network}
                  style={{ marginRight: 10 }}>
                  <Avatar className={classes.NetworkAvatar}>
                    <img src={NETWORK_IMG_URLS[network]} />
                  </Avatar>
                </a>
              ))
            }
          </div>
        }
      </div>
    </div>
  );
};

EmbeddedOwner.propTypes = {
  owner: PropTypes.object.isRequired,
  thumbnail: PropTypes.object.isRequired,
}

export default EmbeddedOwner;
