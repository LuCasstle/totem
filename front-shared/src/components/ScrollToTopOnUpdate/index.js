import React, { Component } from 'react';
import $ from 'jquery';

class ScrollToTopOnUpdate extends Component {
  componentWillReceiveProps = nextProps => {
    if (nextProps.updateId !== this.props.updateId) {
      $('#scrollable-container').scrollTop(0);
    }
  }

  render() {
    return <div></div>
  }
}

export default ScrollToTopOnUpdate;
