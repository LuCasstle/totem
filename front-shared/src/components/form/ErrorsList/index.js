import React from 'react';
import PropTypes from 'prop-types';
import classes from './styles.css';
import { capitalize, map, size } from 'lodash';

const ErrorsList = ({ errorHash }) => {
  if (size(errorHash) > 0) {
    return (
      <div className={classes.ErrorsList}>
        <h6>Des erreurs ont été trouvées: </h6>
        <ul>
          {
            map(errorHash, (_, k) => {
              return (
                <li key={k}>{capitalize(k)} est requis</li>
              )
            })
          }
        </ul>
      </div>
    );
  } else {
    return <div></div>;
  }
}

ErrorsList.propTypes = {
  errorHash: PropTypes.object.isRequired
};

export default ErrorsList;
