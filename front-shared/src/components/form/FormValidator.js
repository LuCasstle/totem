import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { forEach } from 'lodash';

class FormValidator extends Component {
  static propTypes = {
    modelInstance: PropTypes.object.isRequired,
    model: PropTypes.object.isRequired,
    formDetails: PropTypes.object.isRequired,
    FormContent: PropTypes.func.isRequired,
    className: PropTypes.string,

    submitForm: PropTypes.func.isRequired,
    afterSubmit: PropTypes.func.isRequired,
  }

  state = { errorHash: {}, valid: true }
  FormContent = (this.props.formIsClass ? React.createFactory(this.props.FormContent) : this.props.FormContent)

  handleError = field => this.state.errorHash[field] && (<span>Ce champ est requis</span>);

  componentWillReceiveProps = nextProps => {
    if (!this.state.valid && !nextProps.modelInstance.equals(this.props.modelInstance)) {
      this.setState({ valid: true, errorHash: {} });
    }
  }

  validateForm = () => {
    const { modelInstance, model, formDetails } = this.props;

    const errorHash = {};
    let valid = true;

    forEach(formDetails.REQUIRED_FIELDS, key => {
      if (!modelInstance.has(key) || modelInstance.get(key) === '') {
        valid = false;
        errorHash[key] = true;
      }
    });

    this.setState({ valid, errorHash });
    return valid;
  }

  render() {
    const {
      modelInstance, FormContent, className,
      submitForm, afterSubmit
    } = this.props;
    const { valid, errorHash } = this.state;

    return (
      <form
        className={className || ''}
        onSubmit={e => {
          e.preventDefault();
          if (this.validateForm(modelInstance)) {
            submitForm(modelInstance.toJS()).then(afterSubmit);
          }
        }}>
        {
          this.FormContent({
            ...this.state,
            ...this.props,
            handleError: this.handleError,
            errorHash,
            valid
          })
        }
      </form>
    );
  }
}

export default FormValidator;
