import React, { Component } from 'react';
import Button from 'react-toolbox/lib/button';
import Snackbar from 'react-toolbox/lib/snackbar';
import { NOTIFICATION_ERROR, NOTIFICATION_WARNING, NOTIFICATION_SUCCESS } from '../../reducers/notifications';

const TIMEOUT = 4000;
const snackBarTypeMapping = {
  [NOTIFICATION_ERROR]: 'cancel',
  [NOTIFICATION_WARNING]: 'warning',
  [NOTIFICATION_SUCCESS]: 'accept'
}

class NotificationSnackbar extends Component {
  state = {
    active: !!this.props.currentNotification
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.currentNotification) this.setState({ active: true });
  }

  handleSnackbarClick = (event, instance) => {
    this.setState({ active: false });
    setTimeout(() => {
      this.props.consumeNotification();
    }, 2000);
  };

  handleSnackbarTimeout = (event, instance) => {
    this.setState({ active: false });
    setTimeout(() => {
      this.props.consumeNotification();
    }, 2000);
  };

  render () {
    const { currentNotification } = this.props;
    const hasNotification = !!currentNotification;
    const { active } = this.state;

    return (
      <Snackbar
        action='Dismiss'
        active={active}
        label={hasNotification && this.props.currentNotification.get('message')}
        timeout={hasNotification && currentNotification.get('timeout') ? currentNotification.get('timeout') : TIMEOUT}
        onClick={this.handleSnackbarClick}
        onTimeout={this.handleSnackbarTimeout}
        type={hasNotification && snackBarTypeMapping[currentNotification.get('type')]}
      />
    );
  }
}

export default NotificationSnackbar;
