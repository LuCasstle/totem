import { connect } from 'react-redux';
import GlobalLoader from '../components/GlobalLoader';

const mapStateToProps = state => ({
  loading: state.getIn(['notifications', 'globalLoading'])
});

export default connect(mapStateToProps)(GlobalLoader);
