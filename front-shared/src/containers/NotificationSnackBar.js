import { connect } from 'react-redux';
import NotificationSnackBar from '../components/NotificationSnackBar';
import { consumeNotification } from '../actions/notifications';

const mapStateToProps = (state) => ({
  currentNotification: state.getIn(['notifications', 'currentNotification'])
})

const mapDispatchToProps = (dispatch) => ({
  consumeNotification: () => dispatch(consumeNotification())
})

export default connect(mapStateToProps, mapDispatchToProps)(NotificationSnackBar);
