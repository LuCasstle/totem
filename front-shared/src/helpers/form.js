import { forEach } from 'lodash';

export function validateForm (object, model, formDetails) {
  const errorHash = {};
  let valid = true;

  forEach(formDetails.REQUIRED_FIELDS, key => {
    if (!object.has(key) || object.get(key) === '') {
      valid = false;
      errorHash[key] = true;
    }
  });

  this.setState({ valid, errorHash });
  return valid;
}
