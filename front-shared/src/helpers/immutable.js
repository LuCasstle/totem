import { forEach } from 'lodash';

export const arrayToObjectByKey = (arr = [], key = 'id') => {
  const res = {}
  forEach(arr, obj => {
    res[obj[key]] = obj;
  });
  return res;
}

export const onAll = (immutableMap, action) => immutableMap.reduce(action, immutableMap)
