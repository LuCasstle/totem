const baseUrl = 'http://res.cloudinary.com/djv5lkgnv/image/upload/';
const baseUrlLength = baseUrl.length;

export const w600 = url => url.substr(0, baseUrlLength) + 'c_limit,w_600' + url.substr(baseUrlLength - 1, url.length)
export const w1000 = url => url.substr(0, baseUrlLength) + 'c_limit,w_1000' + url.substr(baseUrlLength - 1, url.length)
export const thumb = url => url.substr(0, baseUrlLength) + 't_media_lib_thumb' + url.substr(baseUrlLength - 1, url.length)
export const thumbnail = url => url.substr(0, baseUrlLength) + 'c_thumb,g_face,h_150,w_150' + url.substr(baseUrlLength - 1, url.length)
