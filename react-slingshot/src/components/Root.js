// @flow
import * as React from 'react';
import type { TotemStore } from '../redux/types';
import type { RouterHistory } from 'react-router-dom';
import { ConnectedRouter } from 'react-router-redux';
import { Provider } from 'react-redux';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

import App from './App';

type Props = {
  store: TotemStore,
  history: RouterHistory
};

export default class Root extends React.Component<Props> {
  render() {
    const { store, history } = this.props;
    return (
      <Provider store={store}>
        <ConnectedRouter history={history}>
          <MuiThemeProvider>
            <App />
          </MuiThemeProvider>
        </ConnectedRouter>
      </Provider>
    );
  }
}
