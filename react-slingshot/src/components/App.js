// @flow
import * as React from 'react';
import Routes from '../routes/routes';

type Props = {
  children?: React.Element<*>;
}

class App extends React.Component<Props> {
  render() {
    return <Routes />;
  }
}

export default App;
