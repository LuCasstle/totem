// @flow
import * as Redux from 'redux';

export type State = {};
export type GetState = () => State;
export type Dispatch = () => void | Promise<*>;
export type Action = { type: string };
export type TotemStore = Redux.Store<State, Action, Dispatch>;
