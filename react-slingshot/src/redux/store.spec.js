// @flow
// import * as ActionTypes from '../constants/actionTypes';
import configureStore from './configureStore';

const initialState = {
  routing: {
    location: null,
  }
};

describe('Store', () => {
  beforeAll(() => {
    // Do stuff before all
  });
  afterAll(() => {
    // Do stuff after all
  });

  it('should display results when necessary data is provided', () => {
    const store = configureStore();

    const actions = [];
    actions.forEach(action => store.dispatch(action));

    const actual = store.getState();
    const expected = initialState;

    expect(actual).toEqual(expected);
  });

  it('should handle a flurry of actions', () => {
    const store = configureStore();

    const actions = [];
    actions.forEach(action => store.dispatch(action));

    const moreActions = [];

    moreActions.forEach(action => store.dispatch(action));

    const actual = store.getState();

    // with jest snapshots the above assertion can be replaced with this one line
    // jest will store the value in a file within ./__snapshots__
    // snapshots can/should be committed and reviewed
    // jest will also update snapshot or delete unused ones using the command `npm run test -- -u`
    expect(actual).toMatchSnapshot();
  });
});
