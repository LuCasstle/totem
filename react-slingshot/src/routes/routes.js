// @flow
import React from 'react';
import { Switch, Route } from 'react-router-dom';
import Presentation from './presentation';
import NotFoundPage from './NotFoundPage';

const Routes = () => (
	<Switch>
		<Route path="/presentation" component={Presentation} />
		<Route component={NotFoundPage} />
	</Switch>
);

export default Routes;
