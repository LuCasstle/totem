// @flow
import React from 'react';
import Paper from 'material-ui/Paper'
import { Row, Col } from 'react-flexbox-grid';

const ConstructionEtSceno = () => (
  <Row style={{ marginBottom: 30 }}>
    <Col md={5}>
      <h2>Construction et scéno</h2>
      <p>
        {"Si vous aimez construire, devenez le maçon \
        du festival en créant ou en participant à un \
        projet de construction. Construisez une scène \
        ? La cuisine, un bar, une merveille artistique \
        ? Nous avons un faible pour les constructeurs \
        de toilettes, on aime quand ils sont beaux et \
        confortables. Par contre, laissez aux \
        architectes du Totem le délice de leur \
        exclusivité et ne construisez pas un Totem \
        bis, poil d'anubis ... "}
      </p>
    </Col>
    <Col md={7}>
      <Paper className="image-placeholder" />
    </Col>
  </Row>
);

export default ConstructionEtSceno;
