// @flow
import React from 'react';
import Paper from 'material-ui/Paper'
import TextField from 'material-ui/TextField';
import { Row, Col } from 'react-flexbox-grid';

const Performance = () => (
  <Row className="section">
    <Col md={3} xs={6}>
      <Paper className="image-placeholder" />
    </Col>
    
    <Col md={3} xs={6}>
      <Paper className="image-placeholder" />
    </Col>
    
    <Col md={4} xs={6}>
      <h2>Performance</h2>
      <p>
        {"Offrez vous en pestacle ! Vous voulez lire un \
        poème ou lire l'avenir, faire du stand-up, une \
        chorégraphie loufoque ou royal ? vous toquez à \
        la bonne porte ! Totem est si friand de \
        performances ! Vous avez le meilleur public à \
        Totem. Bienveillant, curieux, averti et rigolard \
        comme après une bonne tartiflette !"}
      </p>
    </Col>
    
    <Col md={2} xs={12} />
  </Row>
);

export default Performance;