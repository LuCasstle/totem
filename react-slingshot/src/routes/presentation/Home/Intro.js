// @flow
import React from 'react';
import Paper from 'material-ui/Paper'
import TextField from 'material-ui/TextField';
import { Row, Col } from 'react-flexbox-grid';

const Intro = () => (
  <Row className="section" id="presentation-intro" style={{ paddingTop: 50 }}>
    <Col md={6} xs={12} mdOffset={1}>
      <Paper zDepth={5}>
        <div style={{
          backgroundImage: "url('https://scontent-bru2-1.xx.fbcdn.net/v/t31.0-8/25542437_491314474585918_2005904043848374863_o.jpg?oh=f3ddbb91cf5795d52ed7ea72ace676ae&oe=5B130024')",
          backgroundPosition: '0px -58px',
          height: 400,
          backgroundSize: '100%'
        }}>
        </div>
      </Paper>
    </Col>
    <Col md={4} xs={12} style={{ paddingLeft: 30 }}>
      <h2>Participez au clafoutis créatif</h2>
      <p>
        {"Totem est une fête créative participative de \
        quelques jours réunissant quelques centaines \
        de participants au début de l\'automne. \
        Chaque participant offre, s\'il le veut, seul ou \
        en groupe, un projet créatif au reste de la \
        fête. Il n'y a pas d'artistes ni de public, il n'y a \
        que des participants (déguisés bien sûr) !\'"}
      </p>
      
      <TextField 
        hintText="Laissez votre email pour etre prévenu du prochain Totem" 
        fullWidth={true} 
        id="let-your-email-1" 
      />
      <p>Comme déjà XXX personnes intéressées !</p>
    </Col>
  </Row>
);

export default Intro;