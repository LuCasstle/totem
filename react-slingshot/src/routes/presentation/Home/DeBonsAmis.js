// @flow
import React from 'react';
import Paper from 'material-ui/Paper'
import { Row, Col } from 'react-flexbox-grid';

const DeBonsAmis = () => (
  <Row style={{ marginBottom: 30 }}>
    <Col md={7}>
      <Paper className="image-placeholder" />
    </Col>
    <Col md={5}>
      <h2>De bons amis</h2>
      <p>
        {"Ah oui, on allait oublier le plus important. Totem \
        c'est avant tout de bons amis. Beaucoup, beaucoup, \
        beaucoup, beaucoup, beaucoup d'amitié. Votre \
        projet peut être simplement d'être l'ami de tous. Et \
        même si vous êtes méchant, ce sera pris pour de \
        l'amitié déguisée. Il pourrait n'y avoir aucun autre \
        projet que celui d'être ami. Tout le monde pourrait \
        jouer aux cartes pendant trois jours, ça ne changerait \
        pas la nature profonde de Totem qui est celle de \
        s'amuser ensemble, de créer ensemble, de découvrir \
        ensemble."}
      </p>
    </Col>
  </Row>
);

export default DeBonsAmis;
