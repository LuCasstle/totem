// @flow
import React from 'react';
import Paper from 'material-ui/Paper'
import { Row, Col } from 'react-flexbox-grid';

const Ateliers = () => (
  <Row className="section">
    <Col md={3} xs={12} />
    
    <Col md={3} xs={6}>
      <Paper className="image-placeholder" />
    </Col>
    
    <Col md={3} xs={6}>
      <h2>Ateliers</h2>
      <p>
        {"Donnez un cours de Slackline, de conduite de \
        drone, de yoga, de danse tzigane ? Maquillez les \
        autres, organisez un débat philosophique ou un \
        foot, un grand jeu, une tombola (s'il vous plaît \
        !), faites tourner un mini cinéma ? Quelque soit \
        votre atelier, il fera de nous de gens heureux"}
      </p>
    </Col>
    
    <Col md={3} xs={12} />
  </Row>
);

export default Ateliers;