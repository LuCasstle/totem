// @flow
import React from 'react';
import Paper from 'material-ui/Paper'
import { Row, Col } from 'react-flexbox-grid';

const OrganisationEtMystique = () => (
  <Row className="section">
    <Col md={6} xs={12}>
      <div style={{ marginBottom: 20 }}>
        <h2>Organisation</h2>
        <p>
        {"Occupez vous des autres à l'infirmerie, \
        accueillez les nouveaux arrivants à \
        l'accueil, faites des rondes pour vérifier \
        que personne ne s'introduit dans Totem \
        par erreur ou effraction, posez des \
        lumières dirigez les gens... Savoir \
        organiser, c'est au moins aussi important \
        que savoir jouer !"}
        </p>
      </div>
      
      <div>
        <h2>Mystique</h2>
        <p>
          {"À la nuit tombée, lorsque la pleine lune rugit, que les pensées \
          frémissents, que le ventre de Totem vous engloutit, dansez \
          autour du feu et célébrez l'amitié, l'optimisme, le partage, la \
          légèreté mystique, l'électricité du moment, la gloire du toimoie"}
        </p>
      </div>
    </Col>
    
    <Col md={6} xs={12}>
      <Paper className="image-placeholder" />
    </Col>
  </Row>
);

export default OrganisationEtMystique;