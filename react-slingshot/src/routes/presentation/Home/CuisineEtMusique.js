// @flow
import React from 'react';
import Paper from 'material-ui/Paper'
import { Row, Col } from 'react-flexbox-grid';

const CuisineEtMusique = () => (
  <Row className="section">
    <Col md={3} xs={6}>
      <h2>Cuisine</h2>
      <p>
        {"Jambalaya aux crevettes, aligot, soupe à \
        l'oignon, crépes bretonnes, curry de \
        l'égumes du chef... Il y a plusieurs \
        merveilles culinaires au palmarès des \
        cuisines collaboratives de Totem, et de \
        sacrés cuisiniers dans les starting blocks ! \
        La devise de Fleury michon \"Bien manger, \
        c'est le début du bonheur\" reprend des \
        couleurs à Totem !"}
      </p>
    </Col>
    
    <Col md={3} xs={6}>
      <Paper className="image-placeholder" />
    </Col>
    
    <Col md={3} xs={6}>
      <Paper className="image-placeholder" />
    </Col>
    
    <Col md={3} xs={6}>
      <h2>Musique</h2>
      <p>
        {"Beaucoup de participants passent de \
        l'autre côté des platines et \
        deviennent les rois de la piste le \
        temps du festival ! Alors préparez vos \
        vyniles ... et affutez vos médiateurs ! \
        Car vous pouvez aussi devenir le roi \
        de la gratte. Il y a toutes sortes de \
        projets musicaux à Totem, faites \
        vous aussi chavirer les popotins !"}
      </p>
    </Col>
  </Row>
);

export default CuisineEtMusique;