// @flow
import React from 'react';
import Paper from 'material-ui/Paper'
import Intro from 'SRC/routes/presentation/Home/Intro';
import ConstructionEtSceno from 'SRC/routes/presentation/Home/ConstructionEtSceno';
import CuisineEtMusique from 'SRC/routes/presentation/Home/CuisineEtMusique';
import Ateliers from 'SRC/routes/presentation/Home/Ateliers';
import Performance from 'SRC/routes/presentation/Home/Performance';
import OrganisationEtMystique from 'SRC/routes/presentation/Home/OrganisationEtMystique';
import DeBonsAmis from 'SRC/routes/presentation/Home/DeBonsAmis';
import Carousel from 'SRC/routes/presentation/components/Carousel';

// <Intro />
// <ConstructionEtSceno />
// <CuisineEtMusique />
// <Ateliers />
// <Performance />
// <OrganisationEtMystique />
// <DeBonsAmis />

const Home = () => (
  <div className="home">
    <div>
      <div className="carrousel">
      <Carousel/>
      </div>
      <div className="seperator"></div>
      <div className="carrousel"><h3>Construction</h3></div>
      <div className="background2"></div>
      <div className="seperator"></div>
      <div className="carrousel"><h3>Construction</h3></div>
      <div className="background3"></div>
    </div>
  </div>
);
export default Home;