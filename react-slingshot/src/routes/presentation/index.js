// @flow
import React from 'react';
import AppBar from 'SRC/routes/presentation/components/Header';
import Footer from 'SRC/routes/presentation/components/Footer';
import { Switch, Route, type Match } from 'react-router-dom';
import { Grid } from 'react-flexbox-grid';
import Home from 'SRC/routes/presentation/Home';
import Understand from 'SRC/routes/presentation/Understand';
import PassedTotems from 'SRC/routes/presentation/PassedTotems';

type Props = {
  match: Match
}

const HomePage = ({ match }: Props) => {
  return (
    <React.Fragment>
      <AppBar />
      
      <Grid fluid>
        <Switch>
          <Route exact path={match.url} component={Home} />
          <Route path={`${match.url}/totems-passees`} component={PassedTotems} />
          <Route path={`${match.url}/comprendre`} component={Understand} />
        </Switch>
        
        <Footer />
      </Grid>
    </React.Fragment>
  );
};

export default HomePage;
