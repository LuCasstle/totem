// @flow
import * as React from 'react';
import Slider from 'react-slick';
import ReactDOM from 'react-dom'

const settings = {
  dots: true
};
    
export default class SimpleSlider extends React.Component {
  render () {
    const settings = {
      dots: true,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1
    };
    
    return (
      <div style={{ margin: '0 auto', padding: '40px', width: '80%', color: '#333', background: '#419be0'
 }}>
      <Slider {...settings}>
        <div><img src='http://placekitten.com/g/400/200' /></div>
        <div><img src='http://placekitten.com/g/400/200' /></div>
        <div><img src='http://placekitten.com/g/400/200' /></div>
        <div><img src='http://placekitten.com/g/400/200' /></div>
      </Slider>
      </div>
    );
  }
}