// @flow
import * as React from 'react';
import { Row, Col } from 'react-flexbox-grid';
import TextField from 'material-ui/TextField';

const CopyRight = () => (
  <div style={{ textAlign: 'center', fontSize: '0.8em', marginTop: 20, opacity: 0.6 }}>
    Copyright Toteam production caca - deal
  </div>
);

const Footer = () => (
  <div className="presentation-footer">
    <Row>
      <Col md={4}>
        <h5>Vous voulez être de la partie?</h5>
        <p>Rejoignez la communauté Facebook</p>
        <TextField hint="Ou laissez votre mail" id="let-your-email-2" />
      </Col>
    </Row>
    
    <CopyRight />
  </div>
);

export default Footer;