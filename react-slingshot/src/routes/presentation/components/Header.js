// @flow
import React from 'react';
import AppBar from 'material-ui/AppBar';
import { Link } from 'react-router-dom';

const HomeAppBar = () => {
  return (
    <AppBar
      className="presentation-app-bar"
      title={<Link to="/presentation">Totem</Link>}
      style={{ backgroundColor: 'transparent' }}
    >
      <ul className="presentation-app-bar-links">
        <li>
          <Link to="/presentation/comprendre">Comprendre</Link>
        </li>
        <li>
          <Link to="/presentation/totems-passees">Totems passées</Link>
        </li>
      </ul>
    </AppBar>
  );
};

export default HomeAppBar;
