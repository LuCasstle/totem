import $ from 'jquery'

export const createOrUpdateOgMetatags = ({ url, type, title, description, image, imageWidth, imageHeight, imageFormat }) => {
  createOrUpdateMetatag('property', 'og:url', url);
  createOrUpdateMetatag('property', 'og:type', type);
  createOrUpdateMetatag('property', 'og:title', title);
  createOrUpdateMetatag('property', 'og:description', description);
  createOrUpdateMetatag('property', 'og:image', image);
  createOrUpdateMetatag('property', 'og:image:width', imageWidth);
  createOrUpdateMetatag('property', 'og:image:height', imageHeight);
  createOrUpdateMetatag('property', 'og:image:format', imageFormat);
}

export const createOrUpdateMetatag = (attrName, attrValue, content) => {
  if (content) {
    if ($(`meta[${attrName}='${attrValue}']`).length > 0) {
      $(`meta[${attrName}='${attrValue}']`).attr("content", content);
    } else {
      $('head').append(`<meta ${attrName}="${attrValue}" content="${content}" />`);
    }
  }
}

export const updateTitle = title => {
  if (document.title !== title) {
    document.title = title;
  }
}

export const getMetatagContent = (attrName, attrValue) => {
  if ($(`meta[${attrName}='${attrValue}']`).length > 0) {
    return $(`meta[${attrName}='${attrValue}']`).attr("content");
  } else { return null; }
}
