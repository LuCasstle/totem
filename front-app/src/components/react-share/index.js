import React from 'react';
import classes from './styles.css';
import { getMetatagContent } from 'src/helpers/DOMManager';

import {
  ShareButtons,
  ShareCounts,
  generateShareIcon,
} from 'react-share';

const {
  FacebookShareButton,
  GooglePlusShareButton,
  TwitterShareButton,
} = ShareButtons;

const {
  FacebookShareCount,
  GooglePlusShareCount
} = ShareCounts;

const FacebookIcon = generateShareIcon('facebook');
const TwitterIcon = generateShareIcon('twitter');
const GooglePlusIcon = generateShareIcon('google');

export const FacebookShare = () => (
  <div className={classes.Network}>
    <FacebookShareButton
      url={window.location.href}
      title={getMetatagContent('property', 'og:title')}
      picture={getMetatagContent('property', 'og:image')}
      className={classes.NetworkShareButton}>
      <FacebookIcon
        size={54}
        round />
    </FacebookShareButton>

    <FacebookShareCount
      url={window.location.href}
      className={classes.NetworkShareCount}>
      {count => count}
    </FacebookShareCount>
  </div>
);

export const TwitterShare = () => (
  <div className={classes.Network}>
    <TwitterShareButton
      url={window.location.href}
      title={getMetatagContent('property', 'twitter:title')}
      className={classes.NetworkShareButton}>
      <TwitterIcon
        size={54}
        round />
    </TwitterShareButton>

    <div className={classes.NetworkShareCount}>
      &nbsp;
    </div>
  </div>
);

export const GooglePlusShare = () => (
  <div className={classes.Network}>
    <GooglePlusShareButton
      url={window.location.href}
      className={classes.NetworkShareButton}>
      <GooglePlusIcon
        size={54}
        round />
    </GooglePlusShareButton>

    <GooglePlusShareCount
      url={window.location.href}
      className={classes.NetworkShareCount}>
      {count => count}
    </GooglePlusShareCount>
  </div>
);
