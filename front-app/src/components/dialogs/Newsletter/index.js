import React from 'react';
import PropTypes from 'prop-types';
import Dialog from 'react-toolbox/lib/dialog';
import classes from './styles.css';
import Logo from 'shared/src/components/Logo/Big';
import Button from 'react-toolbox/lib/button';

const NewsletterDialog = ({ active, closeNewsletterDialog }) => (
  <Dialog
    active={active}
    onEscKeyDown={closeNewsletterDialog}
    onOverlayClick={closeNewsletterDialog}
    className={classes.Dialog}>
    <div className={classes.DialogContentContainer}>
      <Logo height={40}/>

      <h3 className={classes.Signin}>Merci!</h3>
      <h4  className={classes.SubTitle}>
        D'avoir souscrit à la newsletter
      </h4>

      <div style={{ marginBottom: 60 }}>
        <Button onClick={closeNewsletterDialog} primary raised>Fermer</Button>
      </div>

      <p>
        En souscrivant à la newsletter, vous allez être tenu au courant de
        nos meilleurs articles du moment! Vous pourrez à tout moment vous désinscrire.
      </p>
    </div>
  </Dialog>
);

NewsletterDialog.propTypes = {
  active: PropTypes.bool.isRequired,
  closeNewsletterDialog: PropTypes.func.isRequired,
}

export default NewsletterDialog;
