import React from 'react';
import PropTypes from 'prop-types';
import Dialog from 'react-toolbox/lib/dialog';
import classes from './styles.css';
import Logo from 'shared/src/components/Logo/Big';
import { GooglePlusShare, FacebookShare, TwitterShare } from 'src/components/react-share';

const ShareDialog = ({ active, closeDialog }) => (
  <Dialog
    active={active}
    onEscKeyDown={closeDialog}
    onOverlayClick={closeDialog}
    className={classes.Dialog}>
    <div className={classes.DialogContentContainer}>
      <Logo height={40}/>

      <h4 className={classes.Title}>Partagez sur vos réseaux sociaux préférés</h4>
      <h5  className={classes.SubTitle}>
        Partagez notre contenu afin de permettre de bénéficier d'un levier formidable.
      </h5>

      <div className={classes.ShareButtonsContainer}>
        <FacebookShare />
        <GooglePlusShare />
        <TwitterShare />
      </div>

      <p>
        En partageant le contenu que nous publions sur Flykke, vous permettez aux divers projets que nous
        mettons en avant d'être soutenu par d'autres et ainsi de prendre de l'ampleur! <br/>
        N'hésitez pas, ça ne coute rien mais ça peut avoir beaucoup d'influence!
      </p>
    </div>
  </Dialog>
);

ShareDialog.propTypes = {
  active: PropTypes.bool.isRequired,
  closeDialog: PropTypes.func.isRequired,
}

export default ShareDialog;
