import React from 'react';
import classes from './styles.css';
import NavLink from 'shared/src/components/NavLink';
import Logo from 'shared/src/components/Logo/Big';
import { URLS as NETWORK_IMG_URLS } from 'shared/src/constants/socialicons';
import Avatar from 'react-toolbox/lib/avatar';

export default () => (
  <div className={classes.Wrapper}>
    <div className={classes.FooterFirstComponents}>
      <Logo height={30} color={'grey'} />
      <p style={{ marginTop: 10 }}>
        Flykke fait de la publicité votre allié: faites des dons gratuitement,
        financez librement la réussite de ceux en qui vous croyez !
      </p>
    </div>

    <div className={classes.FooterSecondComponents}>
      <ul>
        <li>
          <NavLink
            to={'/commentcamarche'}
            label={'Comment ça marche'}
            additonalClassName={classes.link}
          />
        </li>
        <li>
          <NavLink
            to={'/cgu'}
            label={'CGU'}
            additonalClassName={classes.link}
          />
        </li>
        <li>
          <NavLink
            to={'/transparence'}
            label={'Transparence'}
            additonalClassName={classes.link}
          />
        </li>
        <li>
          <NavLink
            to={'/confidentialite'}
            label={'Confidentialité'}
            additonalClassName={classes.link}
          />
        </li>
      </ul>
    </div>

    <div className={classes.FooterThirdComponents}>
      <p>Rentrez en contact avec nous</p>
      <ul>
        <li>
          <a
            href={'https://www.facebook.com/flykkeOfficial/?ref=br_rs'}
            target='_blank'
            key={'facebook'}>
            <Avatar className={classes.NetworkAvatar}>
              <img src={NETWORK_IMG_URLS.facebook} />
            </Avatar>
          </a>
        </li>
        <li>
          <a
            href={'https://twitter.com/Flykke_Officiel?lang=en'}
            target='_blank'
            key={'twitter'}>
            <Avatar className={classes.NetworkAvatar}>
              <img src={NETWORK_IMG_URLS.twitter} />
            </Avatar>
          </a>
        </li>
        <li>
          <a
            href='mailto:contact@flykke.com'
            key={'email'}>
            <Avatar className={classes.NetworkAvatar}>
              <img src={NETWORK_IMG_URLS.email} />
            </Avatar>
          </a>
        </li>
      </ul>
    </div>
  </div>
)
