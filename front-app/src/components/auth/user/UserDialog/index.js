import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Dialog from 'react-toolbox/lib/dialog';
import Logo from 'shared/src/components/Logo/Big';
import classes from './styles.css';
import { SignOutButton } from "redux-auth/default-theme";
import { DestroyAccountButton } from "redux-auth/default-theme";
import history from 'src/store/history';
import Avatar from 'react-toolbox/lib/avatar';

const AFTER_CLOSE_TIMEOUT = 200;
const AFTER_DECONNECTION = 1000;

class UserDialog extends Component { 
  static propTypes = {
    editingProfile: PropTypes.bool.isRequired,
    active: PropTypes.bool.isRequired,
    user: PropTypes.object,
    isAuthenticated: PropTypes.bool.isRequired,

    closeUserModal: PropTypes.func.isRequired,
  }

  state = { user: null }

  componentWillReceiveProps(nextProps) {
    if (!nextProps.isAuthenticated && this.props.isAuthenticated) {
      this.setState({ user: this.props.user });
      setTimeout(() => this.setState({ user: null }), AFTER_DECONNECTION);
      this.props.closeUserModal();
    }
  }

  render() {
    const {
      editingProfile, active, isAuthenticated,
      closeUserModal
    } = this.props;

    let user = this.props.user || this.state.user;

    return (
      <Dialog
        active={active}
        onEscKeyDown={closeUserModal}
        onOverlayClick={closeUserModal}
        className={classes.Dialog}>
        { user ?
          <div className={classes.DialogContentContainer}>
            <Logo height={40}/>

            <div style={{ marginTop: 40, marginBottom: 20 }}>
              <Avatar className={classes.Avatar} title={user.get('name')} image={user.get('image')} />
            </div>
            <h4 className={classes.UserName}>{user.get('name')}</h4>
            <h6>{user.get('email')}</h6>

            <div className={classes.BottomLinksContainer} style={{ marginTop: 30 }}>
              <SignOutButton
                next={closeUserModal}
                className={classes.DestroyAction}>
                <span>Se déconnecter</span>
              </SignOutButton>


              <DestroyAccountButton
                className={classes.DestroyAction}>
                <span>Supprimer mon compte</span>
              </DestroyAccountButton>
            </div>

            <hr className={classes.FullSeperator} />

            <div className={classes.BottomLinksContainer}>
              <a href='#' onClick={() => {
                closeUserModal();
                setTimeout(() => history.push('/vousavezunprojet'), AFTER_CLOSE_TIMEOUT);
              }}>
                Proposer un projet Flykke
              </a>

              <a href='#' onClick={() => {
                closeUserModal();
                setTimeout(() => history.push('/commentcamarche'), AFTER_CLOSE_TIMEOUT);
              }}>
                Comment ça marche?
              </a>
            </div>
          </div> :
          <div></div>
        }
      </Dialog>
    );
  }
}

export default UserDialog;
