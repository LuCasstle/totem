import React from 'react';
import { OAuthSignInButton } from "redux-auth/material-ui-theme";
import classes from './styles.css';

export const FacebookButton = ({ next, style }) => (
  <OAuthSignInButton
    provider={'facebook'}
    style={style}
    next={next}
    icon={null}>
    <span className={classes.FacebookButtonContent}>
      <img
        src={'/assets/icons/facebook_325x325.png'}
        alt={'FB'}
        width={15}
      />
      <span>Facebook</span>
    </span>
  </OAuthSignInButton>
);

export const GoogleButton = ({ next, style }) => (
  <OAuthSignInButton
    provider={'google'}
    next={next}
    style={style}
    icon={null}>
    <span className={classes.GoogleButtonContent}>
      <img
        src={'/assets/icons/google_128x128.png'}
        alt={'Google'}
        width={15}
      />
      <span>Google</span>
    </span>
  </OAuthSignInButton>
);

export const TwitterButton = ({ next, style }) => (
  <OAuthSignInButton
    provider={'twitter'}
    icon={null}
    style={style}
    next={next}>
    <span className={classes.TwitterButtonContent}>
      <img
        src={'/assets/icons/twitter_192x192.png'}
        alt={'Twitter'}
        width={22}
      />
      <span>Twitter</span>
    </span>
  </OAuthSignInButton>
);
