import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Dialog from 'react-toolbox/lib/dialog';
import classes from './styles.css';
import Logo from 'shared/src/components/Logo/Big';
import { FacebookButton, GoogleButton } from './buttons';

class SigninDialog extends Component {
  static propTypes = {
    active: PropTypes.bool.isRequired,
    userId: PropTypes.string,

    unsetAfterSignin: PropTypes.func.isRequired,
    closeSigninModal: PropTypes.func.isRequired,
    triggerAfterSignin: PropTypes.func.isRequired,
  }

  afterSignin = () => {
    const { userId, closeSigninModal, triggerAfterSignin } = this.props;
    closeSigninModal();
    triggerAfterSignin();
  }

  closeSigninModal = () => {
    const { closeSigninModal, unsetAfterSignin } = this.props;
    unsetAfterSignin();
    closeSigninModal();
  }

  render() {
    const { active, closeSigninModal } = this.props;

    return (
      <Dialog
        active={active}
        onEscKeyDown={this.closeSigninModal}
        onOverlayClick={this.closeSigninModal}
        className={classes.Dialog}>
        <div className={classes.DialogContentContainer}>
          <Logo height={40}/>

          <h3 className={classes.Signin}>Sign in</h3>
          <h4  className={classes.SubTitle}>Révolutionnez votre façon d'aimer sur internet!</h4>

          <div style={{ marginBottom: 60 }}>
            <FacebookButton next={this.afterSignin} style={{ marginRight: 10 }}/>
            <GoogleButton next={this.afterSignin} />
          </div>

          <p>
            Pour utiliser Flykke vous devez accepter nos cookies. En vous connectant avec l'un des liens ci-desssus,
            nous recevrons des informations basiques sur votre profil, nous utiliserons uniquement ces données sur notre
            site pour vous permettre de regarder des pubs plus intéressantes pour vous. Pour plus d'infos consultez
            notre F.A.Q.
          </p>
        </div>
      </Dialog>
    );
  }
}

export default SigninDialog;
