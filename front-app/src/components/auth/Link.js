import React, { Component } from 'react';
import PropTypes from 'prop-types';

class AuthenticatedLink extends Component {
  static propTypes = {
    isAuthenticated: PropTypes.bool.isRequired,
    user: PropTypes.object,

    onAuthenticatedClick: PropTypes.func,
    onUnAuthenticatedClick: PropTypes.func,
    openSigninModal: PropTypes.func.isRequired,
  }

  onClick = () => {
    const {
      isAuthenticated, openSigninModal,
      onAuthenticatedClick, onUnAuthenticatedClick
    } = this.props;

    if (isAuthenticated) {
      if (onAuthenticatedClick) { onAuthenticatedClick() }
    } else {
      if (onUnAuthenticatedClick) { onUnAuthenticatedClick() }
      openSigninModal();
    }
  }

  render = () => {
    const {
      isAuthenticated, openSigninModal, onAuthenticatedClick, onUnAuthenticatedClick, user,
      children, ...rest
    } = this.props;
    return (
      <a
        onClick={this.onClick}
        {...rest}>
        {children}
      </a>
    );
  }
}

export default AuthenticatedLink;
