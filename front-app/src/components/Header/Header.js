import React from 'react'
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import AuthenticatedLink from 'src/containers/auth/Link';
import AppBar from 'react-toolbox/lib/app_bar';
import Navigation from 'react-toolbox/lib/navigation';
import AppBarTheme from './styles.css';
import Logo from 'shared/src/components/Logo/Big';
import Avatar from 'react-toolbox/lib/avatar';
import Chip from 'react-toolbox/lib/chip';
import { notification } from 'shared/src/styles/chip.css';
import FontIcon from 'react-toolbox/lib/font_icon';

const Header = ({
  isAuthenticated, user,
  subscribeToNewsletter, setShouldSubscribeToNewsletter, openUserModal
}) => (
  <AppBar fixed title={
      <Link className={AppBarTheme.titleLink} to={'/'}><Logo height={40}/></Link>
    }
    theme={AppBarTheme}>
    <Navigation className={AppBarTheme.navigationLeft} type="horizontal">
      <Link to={'/vousavezunprojet'}>Vous avez un projet</Link>
    </Navigation>

    <Navigation className={AppBarTheme.navigationRight} type="horizontal">

      {
        (user && !user.get('newsletter')) &&
        <Chip style={{ marginRight: 30 }} className={notification}>
          <AuthenticatedLink
            onAuthenticatedClick={() => subscribeToNewsletter(user.get('id'))}
            onUnAuthenticatedClick={setShouldSubscribeToNewsletter}
            href='#'>
            <FontIcon style={{ width: 30, verticalAlign: 'middle' }}>star_rate</FontIcon>
            <span  style={{ verticalAlign: 'middle' }}>Inscription à la newsletter</span>
          </AuthenticatedLink>
        </Chip>
      }

      { isAuthenticated ?
        <a
          href='#' onClick={openUserModal}>
          <Avatar title={user.get('name')} image={user.get('image')} />
          <span style={{ marginLeft: 10 }}>{ user.get('name') }</span>
        </a> :
        <AuthenticatedLink href='#'>Connexion</AuthenticatedLink>

      }
    </Navigation>
  </AppBar>
);

Header.propTypes = {
  isAuthenticated: PropTypes.bool.isRequired,
  user: PropTypes.object,

  subscribeToNewsletter: PropTypes.func.isRequired,
  setShouldSubscribeToNewsletter: PropTypes.func.isRequired,
  openUserModal: PropTypes.func.isRequired
}

export default Header
