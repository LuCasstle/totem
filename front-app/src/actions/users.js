import {
  SUBSCRIBE_TO_NEWSLETTER, SUBSCRIBE_TO_NEWSLETTER_RESPONSE,
  TOGGLE_NEWSLETTER_DIALOG, UNSET_AFTER_SIGNIN, SET_AFTER_SIGNIN
} from '../constants/actions';
import { pushNotification } from 'shared/src/actions/notifications';
import { NOTIFICATION_SUCCESS } from 'shared/src/reducers/notifications';
import { subscribeToNewsletter as subscribeToAPINewsletter } from 'src/API/users';

export const openNewsletterDialog = () => (dispatch, getState) => {
  if (getState().getIn(['users', 'user', 'newsletter'])) {
    dispatch({
      type: TOGGLE_NEWSLETTER_DIALOG,
      open: true
    });
  }
}

export const closeNewsletterDialog = () => ({
  type: TOGGLE_NEWSLETTER_DIALOG,
  open: false
});

export const subscribeToNewsletter = userId => (dispatch, getState) => {
  const newsletterActive = getState().getIn(['users', 'user', 'newsletter']);

  if (!newsletterActive) {
    dispatch({
      type: SUBSCRIBE_TO_NEWSLETTER
    });

    return subscribeToAPINewsletter(dispatch, userId).then(
      () => {
        setTimeout(() => dispatch(openNewsletterDialog()), 500);
        return dispatch({ type: SUBSCRIBE_TO_NEWSLETTER_RESPONSE, success: true })
      },
      () => dispatch({ type: SUBSCRIBE_TO_NEWSLETTER_RESPONSE, success: false })
    )
  } else {
    return new Promise(resolve => resolve());
  }
}

export const setShouldSubscribeToNewsletter = () => setAfterSignin(subscribeToNewsletter);

export const unsetAfterSignin = () => ({
  type: UNSET_AFTER_SIGNIN
});

export const setAfterSignin = callback => ({
  type: SET_AFTER_SIGNIN,
  callback
})

export const afterSignin = () => (dispatch, getState) => {
  const userId = getState().getIn(['users', 'user', 'id']);
  const afterSigninFn = getState().getIn(['users', 'afterSignin']);

  if (userId && afterSignin) {
    return dispatch(afterSigninFn()).then(res => {
      dispatch(unsetAfterSignin());
      return res;
    });
  } else {
    return new Promise(resolve => resolve());
  }
}
