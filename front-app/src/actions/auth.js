import { TOGGLE_SIGNIN_MODAL, TOGGLE_USER_MODAL } from '../constants/actions';

export const openSigninModal = () => (dispatch, getState) => {
  if (!getState().getIn(['auth', 'user', 'isSignedIn'])) {
    dispatch({
      type: TOGGLE_SIGNIN_MODAL,
      open: true
    });
  }
}

export const closeSigninModal = () => ({
  type: TOGGLE_SIGNIN_MODAL,
  open: false
});

export const openUserModal = () => (dispatch, getState) => {
  if (getState().getIn(['auth', 'user', 'isSignedIn'])) {
    dispatch({
      type: TOGGLE_USER_MODAL,
      open: true
    });
  }
}

export const closeUserModal = () => ({
  type: TOGGLE_USER_MODAL,
  open: false
});
