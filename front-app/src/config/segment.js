const DEV_KEY = 'vibkABM8t7E2t8kPvILbP0B0pD4x1ceS';
const STAGING_KEY = 'd2z9mtfVNjZ0VPSX0dDuX1sJZM8r1ciI';

const get_key = env => {
  switch (env) {
    case 'development': return DEV_KEY;
    case 'staging': return STAGING_KEY;
    case 'production': return STAGING_KEY;
    default: return DEV_KEY;
  }
}

export default () => {
  !function(){var analytics=window.analytics=window.analytics||[];if(!analytics.initialize)if(analytics.invoked)window.console&&console.error&&console.error("Segment snippet included twice.");else{analytics.invoked=!0;analytics.methods=["trackSubmit","trackClick","trackLink","trackForm","pageview","identify","reset","group","track","ready","alias","debug","page","once","off","on"];analytics.factory=function(t){return function(){var e=Array.prototype.slice.call(arguments);e.unshift(t);analytics.push(e);return analytics}};for(var t=0;t<analytics.methods.length;t++){var e=analytics.methods[t];analytics[e]=analytics.factory(e)}analytics.load=function(t){var e=document.createElement("script");e.type="text/javascript";e.async=!0;e.src=("https:"===document.location.protocol?"https://":"http://")+"cdn.segment.com/analytics.js/v1/"+t+"/analytics.min.js";var n=document.getElementsByTagName("script")[0];n.parentNode.insertBefore(e,n)};analytics.SNIPPET_VERSION="4.0.0";
  analytics.load(get_key(NODE_ENV));
  }}();
}
