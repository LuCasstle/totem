export default {
  apiUrl: __SERVER_ADDR__,
  // apiUrl: 'http://localhost:3000',
  authProviderPaths: {
    twitter:    '/auth/twitter',
    facebook:  '/auth/facebook',
    google:    '/auth/google_oauth2'
  }
};
