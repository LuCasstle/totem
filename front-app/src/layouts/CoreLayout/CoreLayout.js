import React from 'react'
import PropTypes from 'prop-types'
import RouteWithSubRoutes from 'shared/src/components/RouteWithSubRoutes';
import Header from 'src/containers/Header';
import Footer from 'src/components/Footer';
import NotificationSnackBar from 'shared/src/containers/NotificationSnackBar';
import GlobalLoader from 'shared/src/containers/GlobalLoader';
import UserDialog from 'src/containers/auth/user/UserDialog';
import SigninDialog from 'src/containers/auth/user/SigninDialog';
import NewsletterDialog from 'src/containers/dialogs/NewsletterDialog';
import ScrollToTop from 'shared/src/components/ScrollToTop';

export const CoreLayout = ({ children, location }) => (
  <ScrollToTop window={true}>
    <div>
      <Header location={location} />
      <div style={{ marginTop: 64 }}>
        { children }
      </div>
      <Footer />
      <GlobalLoader />
      <NotificationSnackBar />
      <UserDialog />
      <SigninDialog />
      <NewsletterDialog />
    </div>
  </ScrollToTop>
)

CoreLayout.propTypes = {
  children: PropTypes.any.isRequired
}

export default CoreLayout;
