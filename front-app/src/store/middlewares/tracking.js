import { createTracker, EventTypes } from 'redux-segment';

const customMapper = {
  mapper: {
    '@@router/LOCATION_CHANGE': EventTypes.page
  }
}

const tracker = createTracker(customMapper);
export default tracker;
