import {
  UPDATE_ADVERTISER_FORM,
  UPDATE_REQUEST_FORM_HASH, POST_REQUEST_FORM, POST_REQUEST_FORM_RESPONSE,
  REQUEST_ALL_DATA, RECEIVE_ALL_DATA
} from '../../constants/actions';
import { arrayToObjectByKey } from 'shared/src/helpers/immutable';
import { Map, fromJS } from 'immutable';
import { OWNER_MODEL } from 'shared/src/constants/models/owner';
import { REQUEST_MODEL } from 'shared/src/constants/models/request';

const initialState = fromJS({
  advertiserForm: {},
  requestForm: {},
  posting: false,
  loading: false,
  loaded: false,
})

export default (state = initialState, action) => {
  switch (action.type) {
    case REQUEST_ALL_DATA:
      return state.set('loading', true);

    case RECEIVE_ALL_DATA:
      return state.set('loading', false).set('loaded', true);

    case UPDATE_ADVERTISER_FORM:
      return state.setIn(['advertiserForm', action.key], action.value);

    case UPDATE_REQUEST_FORM_HASH:
      return state.setIn(['requestForm', action.key], action.value)

    case POST_REQUEST_FORM:
      return state.set('posting', true);

    case POST_REQUEST_FORM_RESPONSE:
      if (action.success) {
        return state.set('posting', false).set('requestForm', Map());
      }

    default:
      return state;
  }
}
