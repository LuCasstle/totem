import { RECEIVE_POSTS, RECEIVE_ALL_DATA } from '../../constants/actions';
import { arrayToObjectByKey } from 'shared/src/helpers/immutable';
import { Map, fromJS } from 'immutable';
import { MEDIA_TYPES, SPECIFIC_TYPES_FOR_MEDIA } from 'shared/src/constants/assets';
import { simplePlurify } from 'shared/src/helpers/text';

const IMAGES = simplePlurify(MEDIA_TYPES.IMAGE);
const VIDEOS = simplePlurify(MEDIA_TYPES.VIDEO);
const THUMBNAILS = simplePlurify(SPECIFIC_TYPES_FOR_MEDIA[MEDIA_TYPES.IMAGE].THUMBNAIL);
const ARTICLES = simplePlurify(SPECIFIC_TYPES_FOR_MEDIA[MEDIA_TYPES.IMAGE].ARTICLE);

const initialState = fromJS({
  [IMAGES]: {
    [ARTICLES]: {},
    [THUMBNAILS]: {},
  },
  [VIDEOS]: {},
});

export default (state = initialState, action) => {
  switch (action.type) {
    case RECEIVE_POSTS:
    case RECEIVE_ALL_DATA:
      if (action.success) {

        const currentImagesState = state.getIn([IMAGES, ARTICLES]);
        const currentThumbnailsState = state.getIn([IMAGES, THUMBNAILS]);

        return state.setIn([IMAGES, ARTICLES], fromJS(arrayToObjectByKey(action.images.articles, 'id')))
                    .setIn([IMAGES, THUMBNAILS], fromJS(arrayToObjectByKey(action.images.thumbnails, 'id')));
      }

    default:
      return state;
  }
}
