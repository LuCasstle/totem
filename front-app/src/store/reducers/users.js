import {
  AUTHENTICATE_COMPLETE, OAUTH_SIGN_IN_COMPLETE,
  SIGN_OUT_COMPLETE, DESTROY_ACCOUNT_COMPLETE,
  TOGGLE_SIGNIN_MODAL, TOGGLE_USER_MODAL, TOGGLE_NEWSLETTER_DIALOG,
  SUBSCRIBE_TO_NEWSLETTER, SUBSCRIBE_TO_NEWSLETTER_RESPONSE,
  UNSET_AFTER_SIGNIN, SET_AFTER_SIGNIN
} from 'src/constants/actions';
import { fromJS, Map } from 'immutable';

const initialState = fromJS({
  user: {},
  afterSignin: null,
  signinModalOpen: false,
  newsletterDialogOpen: false,
  userModalOpen: false,
  editingProfile: false
})

export default (state = initialState, action) => {
  switch (action.type) {
    case AUTHENTICATE_COMPLETE:
    case OAUTH_SIGN_IN_COMPLETE:
      return state.set('user', Map({ 
        ...action.user,
        id: action.user['_id']['$oid']
      }));

    case SIGN_OUT_COMPLETE:
    case DESTROY_ACCOUNT_COMPLETE:
      return state.set('user', Map());

    case TOGGLE_SIGNIN_MODAL:
      return state.set('signinModalOpen', action.open);

    case TOGGLE_USER_MODAL:
      return state.set('userModalOpen', action.open);

    case TOGGLE_NEWSLETTER_DIALOG:
      return state.set('newsletterDialogOpen', action.open);

    case SET_AFTER_SIGNIN:
      return state.set('afterSignin', action.callback);

    case UNSET_AFTER_SIGNIN:
      return state.set('afterSignin', null);

    case SUBSCRIBE_TO_NEWSLETTER_RESPONSE:
      return state.setIn(['user', 'newsletter'], action.success);

    default:
      return state;
  }
}
