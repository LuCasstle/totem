import { RECEIVE_POSTS, RECEIVE_ALL_DATA } from '../../constants/actions';
import { arrayToObjectByKey } from 'shared/src/helpers/immutable';
import { Map, fromJS } from 'immutable';

const initialState = fromJS({
  list: {}
});

export default (state = initialState, action) => {
  switch (action.type) {
    case RECEIVE_POSTS:
    case RECEIVE_ALL_DATA:
      if (action.success) {
        return state.set('list', fromJS(arrayToObjectByKey(action.tags, 'id')));
      }

    default:
      return state;
  }
}

/******* SELECTORS *********/
import { createSelector } from 'reselect'

const getTags = state => state.getIn(['tags', 'list']);
const getPosts = state => state.getIn(['posts', 'list']);

export const getActiveTags = createSelector(
  [getTags, getPosts],
  (tags, posts) => tags.filter(tag => posts.map(p => p.get('tagId')).includes(tag.get('id')))
)
