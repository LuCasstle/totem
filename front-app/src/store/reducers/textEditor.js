import {
  TEMP_SAVE_EDITOR_STATE, INIT_NEW_POST,
  RECEIVE_POSTS, RECEIVE_ALL_DATA
} from '../../constants/actions';
import { fromJS, Map } from 'immutable';
import { EditorState, convertFromRaw } from 'draft-js';
import decorator from 'shared/src/components/TextEditor/decorators';

export const NEW_ID = 'new';

export default (state, action) => {
  switch (action.type) {

    case TEMP_SAVE_EDITOR_STATE:
      return state.setIn(['list', action.postId, 'editorState'], action.editorState);

    case RECEIVE_POSTS:
    case RECEIVE_ALL_DATA:
      return state.set('list', state.get('list').map(post => {
        const rawContent = post.get('content').toJS();
        const newPost = post.set('editorState', EditorState.createWithContent(convertFromRaw(rawContent), decorator))
        return newPost;
      }))

    default:
      return state;
  }
}
