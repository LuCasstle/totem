import { REQUEST_ALL_DATA, RECEIVE_ALL_DATA, } from 'src/constants/actions';
import { arrayToObjectByKey } from 'shared/src/helpers/immutable';
import { fromJS } from 'immutable';

const initialState = fromJS({
  requests: {},
  owners: {},
  loading: false,
  loadedAll: false,
  error: null,
});

export default (state = initialState, action) => {
  let newState = state;

  switch (action.type) {
    case REQUEST_ALL_DATA:
      return state.set('loading', true).set('error', null);

    case RECEIVE_ALL_DATA:
      if (action.success) {
        return state.set('loading', false)
                    .set('loadedAll', true)
                    .set('requests', fromJS(arrayToObjectByKey(action.requests, 'id')))
                    .set('owners', fromJS(arrayToObjectByKey(action.owners, 'id')));
      } else {
        return state.set('loading', false).set('error', action.error);
      }

    default:
      return state;
  }
}
