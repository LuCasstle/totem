import {
  REQUEST_POSTS, RECEIVE_POSTS, RECEIVE_POSTS_BY_TAG,
  SET_FILTER_TAG, RECEIVE_ALL_DATA
} from '../../constants/actions';
import textEditorReducer, { NEW_ID } from './textEditor';
import { arrayToObjectByKey, onAll } from 'shared/src/helpers/immutable';
import { OrderedMap, fromJS, List } from 'immutable';
import { includes, forEach } from 'lodash';

const initialState = fromJS({
  list: {},
  loading: false,
  error: null,
  allPostsLoaded: false,
  filterTag: null
})

export default (state = initialState, action) => {
  switch (action.type) {

    case REQUEST_POSTS:
      return state.set('loading', true).set('error', null);

    case RECEIVE_POSTS:
    case RECEIVE_ALL_DATA:
      if (action.success) {
        let newState;

        newState = state.set('loading', false)
                        .set('allPostsLoaded', true)
                        .set('list', OrderedMap(fromJS(arrayToObjectByKey(action.posts, 'id'))));

        return textEditorReducer(newState, action);
      } else {
        return state.set('loading', false).set('error', action.error);
      }

    case SET_FILTER_TAG:
      return state.set('filterTag', action.tagId);

    default:
      return state;
  }
}

/******* SELECTORS *********/
import { createSelector } from 'reselect'
import moment from 'moment';

const getFilter = state => state.getIn(['posts', 'filterTag']);
const getPosts = state => state.getIn(['posts', 'list']);

const getVisiblePosts = createSelector(
  [getPosts, getFilter],
  (posts, filterTag) => filterTag ? posts.filter(post => post.get('tagId') === filterTag) : posts
)

export const getSortedPosts = createSelector(
  getVisiblePosts,
  posts => posts.sortBy(post => moment(post.get('createdAt'))).reverse()
)
