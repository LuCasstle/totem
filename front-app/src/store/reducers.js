import { combineReducers } from 'redux-immutablejs'
import routeReducer from './routing';
import { authStateReducer } from "redux-auth";
import postsReducer from './reducers/posts';
import projectsReducer from './reducers/projects';
import usersReducer from './reducers/users';
import globalAppReducer from './reducers/global';
import assetsReducer from './reducers/assets';
import tagsReducer from './reducers/tags';
import notificationsReducer from 'shared/src/reducers/notifications';
import uuid from 'uuid/v1';

// Otherwise this reducer doesn't work with redux-immutable
const auth = (state = {}, action) => authStateReducer(fromJS(state), action);

export const makeRootReducer = (asyncReducers) => {
  const rootReducer = combineReducers({
    routing: routeReducer,
    auth: authStateReducer,
    posts: postsReducer,
    assets: assetsReducer,
    tags: tagsReducer,
    projects: projectsReducer,
    global: globalAppReducer,
    users: usersReducer,
    notifications: notificationsReducer,
    ...asyncReducers
  })

  const reduxAuthPatchedReducer = (state, action) => {
    const returnedState = rootReducer(state, action).set('updateUid', uuid());
    Object.defineProperty(returnedState, 'auth', { get: function() { return this.get('auth'); } });
    return returnedState;
  }

  return reduxAuthPatchedReducer;
}

export const injectReducer = (store, { key, reducer }) => {
  if (Object.hasOwnProperty.call(store.asyncReducers, key)) return

  store.asyncReducers[key] = reducer
  store.replaceReducer(makeRootReducer(store.asyncReducers))
}

export default makeRootReducer
