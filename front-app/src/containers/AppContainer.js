import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { ConnectedRouter } from 'react-router-redux'
import history from '../store/history';
import { Provider } from 'react-redux'
import normalize from 'normalize.css'
import globals from 'shared/src/styles/globals.css'
import { Route } from 'react-router';
import RouteWithSubRoutes from 'shared/src/components/RouteWithSubRoutes';

class AppContainer extends Component {
  static propTypes = {
    store  : PropTypes.object.isRequired,
    routes: PropTypes.array.isRequired
  }

  shouldComponentUpdate () {
    return false
  }

  render () {
    const { routes, store } = this.props

    return (
      <Provider store={store}>
        <ConnectedRouter history={history}>
          <div>
            { routes.map((route, i) => (
              <RouteWithSubRoutes key={i} {...route}/>
            )) }
          </div>
        </ConnectedRouter>
      </Provider>
    )
  }
}

export default AppContainer
