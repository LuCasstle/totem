import { connect } from 'react-redux';
import { openUserModal } from 'src/actions/auth';
import { setShouldSubscribeToNewsletter, subscribeToNewsletter } from 'src/actions/users';
import Header from '../components/Header';

const mapStateToProps = state => ({
  isAuthenticated: state.getIn(['auth', 'user', 'isSignedIn']),
  user: state.getIn(['users', 'user']),
});

const mapDispatchToProps = dispatch => ({
  openUserModal: () => dispatch(openUserModal()),
  setShouldSubscribeToNewsletter: () => dispatch(setShouldSubscribeToNewsletter()),
  subscribeToNewsletter: userId => dispatch(subscribeToNewsletter(userId))
})

export default connect(mapStateToProps, mapDispatchToProps)(Header);
