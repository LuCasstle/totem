import { connect } from 'react-redux';
import { closeNewsletterDialog } from 'src/actions/users';
import NewsletterDialog from 'src/components/dialogs/Newsletter/index.js';

export const mapStateToProps = state => ({
  active: state.getIn(['users', 'newsletterDialogOpen'])
});

export const mapDispatchToProps = dispatch => ({
  closeNewsletterDialog: () => dispatch(closeNewsletterDialog()),
});

export default connect(mapStateToProps, mapDispatchToProps)(NewsletterDialog);
