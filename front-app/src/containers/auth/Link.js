import { connect } from 'react-redux';
import AuthenticatedLink from 'src/components/auth/Link';
import { mapStateToProps, mapDispatchToProps } from './index.js'

export default connect(mapStateToProps, mapDispatchToProps)(AuthenticatedLink);
