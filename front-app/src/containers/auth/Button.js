import { connect } from 'react-redux';
import Button from 'react-toolbox/lib/button';
import { mapStateToProps } from './index.js'

export default connect(mapStateToProps)(Button);
