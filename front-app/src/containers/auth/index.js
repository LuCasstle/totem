import { openSigninModal } from 'src/actions/auth';

export const mapStateToProps = state => ({
  isAuthenticated: state.getIn(['auth', 'user', 'isSignedIn']),
  user: state.getIn(['users', 'user']),
});

export const mapDispatchToProps = dispatch => ({
  openSigninModal: () => dispatch(openSigninModal())
});
