import { connect } from 'react-redux';
import { closeSigninModal } from 'src/actions/auth';
import { unsetAfterSignin, afterSignin } from 'src/actions/users';
import SigninDialog from 'src/components/auth/user/SigninDialog';

export const mapStateToProps = state => ({
  active: state.getIn(['users', 'signinModalOpen']),
  userId: state.getIn(['users', 'user', 'id'])
});

export const mapDispatchToProps = dispatch => ({
  closeSigninModal: () => dispatch(closeSigninModal()),
  unsetAfterSignin: () => dispatch(unsetAfterSignin()),
  triggerAfterSignin: () => dispatch(afterSignin())
});

export default connect(mapStateToProps, mapDispatchToProps)(SigninDialog);
