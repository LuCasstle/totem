import { connect } from 'react-redux';
import { closeUserModal } from 'src/actions/auth';
import UserDialog from 'src/components/auth/user/UserDialog';

export const mapStateToProps = state => ({
  editingProfile: state.getIn(['users', 'editingProfile']),
  active: state.getIn(['users', 'userModalOpen']),
  user: state.getIn(['auth', 'user', 'attributes']),
  isAuthenticated: state.getIn(['auth', 'user', 'isSignedIn']),
});

export const mapDispatchToProps = dispatch => ({
  closeUserModal: () => dispatch(closeUserModal())
});

export default connect(mapStateToProps, mapDispatchToProps)(UserDialog);
