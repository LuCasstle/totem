import React, {Component} from 'react';

export default class ValentinComponent extends Component {
	state = {
		backgroundColor: 'white'
	};

	handleClick = () => {
		this.setState({
			backgroundColor: this.state.backgroundColor === 'white' ? 'grey' : 'white'
		});
		console.log('allo?');
		this.props.afterClick();
	}

	render() {
		return (
			<div style={{ backgroundColor: this.state.backgroundColor }}>
				<h1>{this.props.title}</h1>

				<p>
					Super Composant
				</p>

				<button onClick={this.handleClick}>Valentin button</button>
			</div>
		)
	}
}
