import React from 'react'
import ReactDOM from 'react-dom'
import createStore from './store/createStore'
import AppContainer from './containers/AppContainer'
import { fromJS } from 'immutable'
import { configure } from "redux-auth";
import reduxAuthConfig from './config/reduxAuth';

import injectTapEventPlugin from 'react-tap-event-plugin';
injectTapEventPlugin();

import loadMomentLocale from 'shared/src/helpers/moment';
loadMomentLocale();

import initSegment from './config/segment';
initSegment();

// ========================================================
// Store Instantiation
// ========================================================
const initialState = fromJS(window.__INITIAL_STATE__)
const store = createStore(initialState)

// ========================================================
// Render Setup
// ========================================================
const MOUNT_NODE = document.getElementById('root')

let initialLoad = true;
let renderApp = (routes) => {
  return new Promise(resolve => setTimeout(() => {
    if (initialLoad) {
      initialLoad = false;
      store.dispatch(configure(
        reduxAuthConfig,
        { clientOnly: true }
      )).then(({ redirectPath, blank } = {}) => {
        if (blank) {
          resolve(<noscript />)
        } else {
          resolve(<AppContainer store={store} routes={routes} />);
        }
      });
    } else {
      resolve(<AppContainer store={store} routes={routes} />)
    }
  }, 100))
}

let render = () => {
  const routes = require('./routes/index').default(store)

  renderApp(routes).then(appComponent => {
    ReactDOM.render(
      appComponent,
      MOUNT_NODE
    )
  })
}

// This code is excluded from production bundle
if (__DEV__) {
  if (module.hot) {

    // Development render functions
    const newRender = render
    const renderError = (error) => {
      const RedBox = require('redbox-react').default

      ReactDOM.render(<RedBox error={error} />, MOUNT_NODE)
    }

    // Wrap render in try/catch
    render = () => {
      try {
        newRender()
      } catch (error) {
        console.error(error)
        renderError(error)
      }
    }

    // Setup hot module replacement
    module.hot.accept('./routes/index', () =>
      setImmediate(() => {
        ReactDOM.unmountComponentAtNode(MOUNT_NODE)
        render()
      })
    )
  }
}

// ========================================================
// Go!
// ========================================================
render()
