import React from 'react';
import appRoutes from './App';
import RouteWithSubRoutes from 'shared/src/components/RouteWithSubRoutes';
import { Route, Switch } from 'react-router-dom';
import NoMatch from 'src/components/NoMatch';

const BaseComponent = ({ routes }) => {
  return (
    <Switch>
      { routes.map((route, i) => (
        <RouteWithSubRoutes key={i} {...route}/>
      )) }

      <Route component={NoMatch}/>
    </Switch>
  );
}

export default (store) => [{
  component: BaseComponent,
  path: '/',
  routes: [
    ...appRoutes
  ]
}]
