import React from 'react';
import PropTypes from 'prop-types';
import Navigation from 'react-toolbox/lib/navigation';
import styles from './styles.css';
import { map } from 'lodash';

const Nav = ({ tags, filterTag, setFilterTag }) => (
  <Navigation className={styles.Navigation}>
    <a
      className={(!filterTag && styles.active)}
      onClick={() => setFilterTag()}
      key={'null'}>
      All
    </a>

    {
      map(tags.toArray(), tag => (
        <a
          className={((filterTag == tag.get('id')) && styles.active)}
          onClick={() => setFilterTag(tag.get('id'))}
          key={tag.get('id')}>
          { tag.get('name') }
        </a>
      ))
    }
  </Navigation>
)

Nav.propTypes = {
  tags: PropTypes.object.isRequired,
  filterTag: PropTypes.string,
  setFilterTag: PropTypes.func.isRequired
};

export default Nav;
