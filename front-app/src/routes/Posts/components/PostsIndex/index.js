import React, { Component } from 'react';
import PropTypes from 'prop-types';
import DetectScrollWrapper from 'shared/src/components/DetectScrollWrapper';
import Navigation from './Navigation';
import Post from './Post';
import styles from './styles.css';
import { Link } from 'react-router-dom';
import Subheader from 'shared/src/components/SubHeader';

class PostsIndex extends Component {
  static propTypes = {
    posts: PropTypes.object.isRequired,
    images: PropTypes.object.isRequired,
    tags: PropTypes.object.isRequired,
    filterTag: PropTypes.string,
    setFilterTag: PropTypes.func.isRequired
  }

  render() {
    const { 
      posts, images, tags, filterTag,
      setFilterTag
    } = this.props;

    let i = 0;

    return (
      <div>
        <Subheader>
          <h2 className={styles.SubHeaderTitle}>Financez gratuitement ce que vous aimez en regardant de la pub!</h2>
          <div className={styles.commentCaMarcheButtonWrapper}>
            <Link className={styles.commentCaMarcheButton} to={'/commentcamarche'}>Comment ça marche ?</Link>
          </div>
        </Subheader>

        <div className={styles.PostsIndexContainer}>

          <Navigation
            tags={tags}
            setFilterTag={setFilterTag}
            filterTag={filterTag}
          />

          <div className={styles.PostsListContainer}>
            {
              posts.toArray().map(post => (
                <Post
                  post={post}
                  image={images.get(post.get('imageId'))}
                  key={post.get('id')}
                  first={(i++) === 0}
                  tag={tags.get(post.get('tagId'))}
                />
              ))
            }
          </div>
        </div>
      </div>
    )
  }
}

export default PostsIndex;
