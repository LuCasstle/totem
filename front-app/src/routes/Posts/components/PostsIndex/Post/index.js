import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { w600, w1000 } from 'shared/src/helpers/cloudinary';
import { Card, CardMedia, CardTitle, CardText, CardActions } from 'react-toolbox/lib/card';

import { Post as PostClass } from '../styles.css';
import styles from './styles.css';

const PostCard = ({ post, image, tag, first }) => {
  return (
    <div  className={PostClass}>
      <Card style={{width: 'auto'}}>
        <span className={styles.TagFlag}>{tag.get('name')}</span>
        <CardMedia
          aspectRatio="wide"
          image={first ? w1000(image.getIn(['srcInfo', 'url'])) : w600(image.getIn(['srcInfo', 'url']))}
        />
        <CardTitle
          title={
            <Link to={`/articles/${post.get('id')}`} className={styles.title}>
              { post.get('title') }
            </Link>
          }
        />
        <CardText className={styles.PostDescription}>{post.get('description')}</CardText>
      </Card>
    </div>
  );
}

PostCard.propTypes = {
  post: PropTypes.object.isRequired,
  image: PropTypes.object.isRequired,
  first: PropTypes.bool.isRequired,
  tag: PropTypes.object.isRequired,
}

export default PostCard;
