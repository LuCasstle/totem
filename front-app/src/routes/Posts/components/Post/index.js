import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Redirect } from 'react-router-dom';
import PostView from 'shared/src/components/Post/View';
import ShareDialog from 'src/components/dialogs/Share';

class Post extends Component {
  static propTypes = {
    post: PropTypes.object,
    image: PropTypes.object,
    tag: PropTypes.object,
    owner: PropTypes.object,
    request: PropTypes.object,
    loaded: PropTypes.bool.isRequired,
    thumbnail: PropTypes.object,

    afterMount: PropTypes.func.isRequired,
    beforeUnmount: PropTypes.func.isRequired
  }

  state = { shareDialogOpen: false }
  toggleShareDialog = () => this.setState({ shareDialogOpen: !this.state.shareDialogOpen });

  componentDidMount() { this.props.afterMount(this.props.post, this.props.image) }
  componentWillUnmount() { this.props.beforeUnmount() }

  render() {
    const { post, loaded } = this.props;

    if (!post && loaded) {
      return (
        <Redirect to={{
          pathname: '/',
          state: { from: this.props.location }
        }}/>
      );
    }

    return (
      <div>
        <ShareDialog
          active={this.state.shareDialogOpen}
          closeDialog={this.toggleShareDialog}
        />

        <PostView
          {...this.props}
          onShareClick={this.toggleShareDialog}
        />
      </div>
    );
  }
}

export default Post;
