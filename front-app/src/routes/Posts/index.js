import GlobalPostContainer from './containers';
import PostContainer from './containers/Post';
import PostsIndexContainer from './containers/PostsIndex';

// Sync route definition
export default [{
  component: GlobalPostContainer,
  routes: [
    {
      path: '/',
      exact: true,
      component: PostsIndexContainer
    },
    {
      path: '/articles/:postId',
      component: PostContainer
    }
  ]
}]
