import { REQUEST_POSTS, RECEIVE_POSTS, SET_FILTER_TAG } from 'src/constants/actions';
import { getPosts as getAPIPosts } from 'src/API/posts';

export const getPosts = () => dispatch => {
  dispatch({
    type: REQUEST_POSTS
  });

  return getAPIPosts(dispatch).then(
    ({ posts, images, tags, owners }) => dispatch({ type: RECEIVE_POSTS, posts, images, tags, owners, success: true }),
    error => dispatch({ type: RECEIVE_POSTS, success: false, error, message: 'There was an error' })
  )
}

export const setFilterTag = tagId => ({
  type: SET_FILTER_TAG,
  tagId
})
