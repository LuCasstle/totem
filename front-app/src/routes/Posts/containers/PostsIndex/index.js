import { connect } from 'react-redux';
import { setFilterTag } from '../../actions';
import PostsIndex from '../../components/PostsIndex';
import { MEDIA_TYPES, SPECIFIC_TYPES_FOR_MEDIA } from 'shared/src/constants/assets';
import { simplePlurify } from 'shared/src/helpers/text';
import { getSortedPosts } from 'src/store/reducers/posts';
import { getActiveTags } from 'src/store/reducers/tags';

const IMAGE = MEDIA_TYPES.IMAGE;
const ARTICLE = SPECIFIC_TYPES_FOR_MEDIA[IMAGE].ARTICLE;

const mapStateToProps = state => {
  const filterTag = state.getIn(['posts', 'filterTag']);

  return {
    posts: getSortedPosts(state),
    images: state.getIn(['assets', simplePlurify(IMAGE), simplePlurify(ARTICLE)]),
    tags: getActiveTags(state),
    filterTag,
  }
};

const mapDispatchToProps = dispatch => ({
  setFilterTag: tagId => dispatch(setFilterTag(tagId))
});

export default connect(mapStateToProps, mapDispatchToProps)(PostsIndex);
