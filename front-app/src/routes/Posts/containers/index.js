import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getPosts } from '../actions';
import RouteWithSubRoutes from 'shared/src/components/RouteWithSubRoutes';

class GlobalPostComponent extends Component {
  componentWillMount() {
    const { loaded, getPosts } = this.props;
    if (!loaded) getPosts();
  }

  render() {
    return (
      <div>
        { this.props.routes.map((route, i) => (
          <RouteWithSubRoutes key={i} {...route}/>
        )) }
      </div>
    )
  }
}

const mapStateToProps = state => ({
  loaded: state.getIn(['posts', 'allPostsLoaded'])
});

const mapDispatchToProps = dispatch => ({
  getPosts: () => dispatch(getPosts())
})

export default connect(mapStateToProps, mapDispatchToProps)(GlobalPostComponent);
