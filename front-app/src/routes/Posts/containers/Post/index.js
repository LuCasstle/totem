import { connect } from 'react-redux';
import Post from '../../components/Post';
import { createOrUpdateOgMetatags, updateTitle } from 'src/helpers/DOMManager';
import { MEDIA_TYPES, SPECIFIC_TYPES_FOR_MEDIA } from 'shared/src/constants/assets';
import { simplePlurify } from 'shared/src/helpers/text';

const IMAGE = MEDIA_TYPES.IMAGE;
const ARTICLE = SPECIFIC_TYPES_FOR_MEDIA[IMAGE].ARTICLE;
const THUMBNAIL = SPECIFIC_TYPES_FOR_MEDIA[IMAGE].THUMBNAIL;

const mapStateToProps = (state, { match }) => {
  const post = state.getIn(['posts', 'list', match.params.postId]);
  const loaded = state.getIn(['global', 'loaded']);
  if (!post) return { loaded };

  const owner = state.getIn(['projects', 'owners', post.get('projectOwnerId')]);

  return {
    post,
    image: state.getIn(['assets', simplePlurify(IMAGE), simplePlurify(ARTICLE), post.get('imageId')]),
    tag: state.getIn(['tags', 'list', post.get('tagId')]),
    owner,
    thumbnail: owner && state.getIn(['assets', simplePlurify(IMAGE), simplePlurify(THUMBNAIL), owner.get('imageId')]),
    request: state.getIn(['projects', 'requests', post.get('projectRequestId')]),
    loaded
  }
}

const mapDispatchToProps = () => ({
  afterMount: (post, image) => {
    if (post && post.get('id')) {
      createOrUpdateOgMetatags({
        url: window.location.href,
        type: 'article',
        title: post.get('title'),
        description: post.get('description'),
        image: image.getIn(['srcInfo', 'url']),
        imageWidth: image.getIn(['srcInfo', 'width']),
        imageHeight: image.getIn(['srcInfo', 'height']),
        imageFormat: image.getIn(['srcInfo', 'format']),
      });

      updateTitle(post.get('title'));
    }
  },

  beforeUnmount: () => updateTitle('Flykke')
})

export default connect(mapStateToProps, mapDispatchToProps)(Post);
