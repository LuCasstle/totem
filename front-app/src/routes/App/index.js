import App from './containers/AppContainer';
import postsRoutes from '../Posts';

import CommentCaMarche from './components/static/CommentCaMarche';
import CGU from './components/static/CGU';
import Confidentialite from './components/static/Confidentialite';
import Transparence from './components/static/Transparence/index.js';
import PostRequestForm from './containers/PostRequestForm/index.js';
// import AdvertiserForm from './containers/AdvertiserForm';

// Sync route definition
export default [{
  component: App,
  path: '/',
  routes: [
    {
      path: '/commentcamarche',
      component: CommentCaMarche
    },
    {
      path: '/cgu',
      component: CGU
    },
    {
      path: '/confidentialite',
      component: Confidentialite
    },
    {
      path: '/transparence',
      component: Transparence
    },
    {
      path: '/vousavezunprojet',
      component: PostRequestForm
    },
    // {
    //   path: '/devenirannonceur',
    //   component: AdvertiserForm
    // },
    ...postsRoutes
  ]
}]
