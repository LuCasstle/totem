import React from 'react';
import { connect } from 'react-redux';
import App from '../components/App';
import { getData } from '../actions';

const mapStateToProps = state => ({
  loading: state.getIn(['global', 'loading']),
  loaded: state.getIn(['global', 'loaded']),
});

const mapDispatchToProps = dispatch => ({
  getData: () => dispatch(getData())
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
