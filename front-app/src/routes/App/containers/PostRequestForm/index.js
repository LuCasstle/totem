import React from 'react';
import { connect } from 'react-redux';
import { updateRequestFormHash, postRequestForm } from '../../actions/index.js';
import PostRequestForm from '../../components/PostRequestForm/index.js';
import history from 'src/store/history';

const mapStateToProps = state => ({
  requestForm: state.getIn(['global', 'requestForm'])
})

const mapDispatchToProps = dispatch => ({
  updateForm: (name, value) => dispatch(updateRequestFormHash(name, value)),
  submitForm: formParams => dispatch(postRequestForm(formParams)),
  afterSubmit: () => history.push('/')
})

export default connect(mapStateToProps, mapDispatchToProps)(PostRequestForm);
