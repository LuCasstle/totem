import React from 'react';
import { connect } from 'react-redux';
import { updateAdvertiserForm, postAdvertiserForm } from '../../actions/index.js';
import AdvertiserForm from '../../components/AdvertiserForm/index.js';

const mapStateToProps = state => ({
  form: state.getIn(['global', 'advertiserForm'])
})

const mapDispatchToProps = dispatch => ({
  updateForm: (name, value) => dispatch(updateAdvertiserForm(name, value)),
  submitForm: form => dispatch(postAdvertiserForm(form.toJS()))
})

export default connect(mapStateToProps, mapDispatchToProps)(AdvertiserForm);
