import {
  UPDATE_ADVERTISER_FORM, UPDATE_REQUEST_FORM_HASH,
  POST_REQUEST_FORM, POST_REQUEST_FORM_RESPONSE,
  POST_ADVERTISER_FORM, POST_ADVERTISER_FORM_RESPONSE,
  REQUEST_ALL_DATA, RECEIVE_ALL_DATA
} from 'src/constants/actions';
import { setGlobalLoading, unsetGlobalLoading } from 'shared/src/actions/notifications';
import {
  postAdvertiserForm as postAPIAdvertiserForm,
  postAPIRequestForm as postAPIRequestForm,
  getData as getAPIData
} from 'src/API/global';
import { pushNotification } from 'shared/src/actions/notifications';
import { NOTIFICATION_SUCCESS } from 'shared/src/reducers/notifications';

export const updateAdvertiserForm = (key, value) => ({
  type: UPDATE_ADVERTISER_FORM,
  key, value
});

export const updateRequestFormHash = (key, value) => ({
  type: UPDATE_REQUEST_FORM_HASH,
  key, value
});

export const postAdvertiserForm = form => dispatch => {
  dispatch({ type: POST_ADVERTISER_FORM });

  return postAPIAdvertiserForm(dispatch, form).then(
    () => dispatch({ type: POST_ADVERTISER_FORM_RESPONSE, success: true }),
    error => dispatch({ type: POST_ADVERTISER_FORM_RESPONSE })
  )
}

export const postRequestForm = formParams => dispatch => {
  dispatch({ type: POST_REQUEST_FORM });

  return postAPIRequestForm(dispatch, { createdBy: 'user', ...formParams }).then(
    () => {
      dispatch(pushNotification({
        notificationType: NOTIFICATION_SUCCESS,
        message: 'Votre demande a bien été prise en compte, nous revenons vers vous au plus vite!',
        timeout: 10000
      }));

      dispatch({ type: POST_REQUEST_FORM_RESPONSE, success: true })
    },
    error => dispatch({ type: POST_REQUEST_FORM_RESPONSE })
  )
}

export const getData = () => dispatch => {
  dispatch({
    type: REQUEST_ALL_DATA
  });

  dispatch(setGlobalLoading());
  return getAPIData(dispatch).then(
    ({ requests, owners, posts, tags, assets }) => {
      dispatch(unsetGlobalLoading());
      const { images, videos } = assets;

      return dispatch({
        type: RECEIVE_ALL_DATA,
        requests, owners, posts, tags, images, videos,
        success: true
      });
    },
    error => dispatch({ type: RECEIVE_ALL_DATA, success: false, error, message: 'There was an error' })
  )
}
