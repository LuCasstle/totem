import React from 'react';
import classes from '../styles.css';
import Subheader from 'shared/src/components/SubHeader';

const Transparence = () => (
  <div>
    <Subheader type='centered'>
      <h2 style={{ margin: 0 }} className={classes.SubHeaderTitle}>Transparence</h2>
    </Subheader>

    <div className={classes.ContentWrapper}>
      <div className={classes.SectionHeader}>
        <p>
          Notre objectif est de libérer la créativité et le soutien aux
          causes qui vous sont chères de la contrainte monétaire.
          Pour cela notre politique est de vous permettre d’allouer
          à votre guise les revenus publicitaires que nous générons sur Flykke.
        </p>
      </div>

      <div className={classes.section} style={{ marginTop: 30 }}>
        <p>
          Lorsque vous cliquez sur un bouton Flykke et que vous validez
          le visionnage de la publicité, nous nous engageons à reverser
          à l’auteur de l’œuvre ou du projet 70% de ce
          qu’a payé l’annonceur pour cet affichage.
        </p>

        <p>
          Nos partenaires, créateurs comme ONG, s’engagent à
          utiliser les fonds redistribués pour les projets
          explicités sur Flykke. De plus, nos ONG partenaires
          publient annuellement leurs résultats et
          justifient l’utilisation de leurs fonds.
        </p>

        <p>
          Pour information, les 30% restants sont alloués au
          développement de notre plateforme (principalement pour développer
          notre offre, trouver de nouveaux partenaires et
          créer des outils plus performants dans le but de vous faire
          profiter d’une expérience toujours plus satisfaisante !).
        </p>
      </div>
    </div>
  </div>
);

export default Transparence
