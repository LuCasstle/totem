import React from 'react';
import classes from '../styles.css';
import Subheader from 'shared/src/components/SubHeader';
import Link from 'react-toolbox/lib/link';

const CGU = () => (
  <div>
    <Subheader type='centered'>
      <h2 className={classes.SubHeaderTitle} style={{ margin: 0 }}>Conditions générales d'utilisation</h2>
    </Subheader>

    <div className={classes.ContentWrapper}>
      <div className={classes.SectionHeader}>
        <h3 className={classes.sectionTitle}>
          Bienvenue sur Flykke !
        </h3>

        <p>
          La confiance est la fondation de toute bonne relation,
          aussi il est important que vous lisiez, compreniez et
          acceptiez les conditions générales d’utilisation du site.
          Si vous avez la moindre question contactez nous !
        </p>

        <p>
          La poursuite de la navigation sur ce site vaut acceptation
          sans réserve des dispositions et conditions d'utilisation qui suivent.
        </p>

        <p>
          La version actuellement en ligne de ces conditions d'utilisation
          est la seule opposable pendant toute la durée d'utilisation
          du site et jusqu'à ce qu'une nouvelle version la remplace.
        </p>
      </div>

      <div className={classes.section}>
        <h3 className={classes.sectionTitle}>Informations légales</h3>

        <p>
          Site (ci-après « le site ») : www.flykke.com
        </p>

        <p>
          Éditeur (ci-après « l'éditeur ») : FLYKKE, Société par Actions Simplifiée,
          au capital de 52 000 euros dont le siège social est situé 35 chemin de Monteau,
          30400 Villeneuve les Avignon. FLYKKE est immatriculée au RCS,
          représentée par Cyril CHEHOVAH, en ses qualités de Président et de fondateur.
        </p>

        <p>
          Adresse de contact :
          <Link className={classes.link} href='mailto:contact@flykke.com' icon='email'>contact@flykke.com</Link>
        </p>

        <p>
          Les présentes Conditions Générales d'Utilisation ont pour objet de
          définir les dispositions qui s'appliquent aux Visiteurs personnes
          physiques ou morales dans le cadre de l’accès et l'utilisation
          des informations et services du site www.flykke.com. et ce quel
          que soit leur statut : Visiteur, Utilisateur, donateur tel que
          définis à l’Article 3 ci-dessous.
        </p>
      </div>

      <div className={classes.section}>
        <h3 className={classes.sectionTitle}>Utiliser Flykke</h3>

        <p>
          Tout accès ou utilisation du site vaut acceptation sans réserves des présentes CGU.
          La Société se réserve la possibilité de modifier en tout ou partie
          ces termes d’utilisation afin de les adapter aux évolutions de son exploitation,
          et/ou à l'évolution de la législation et/ou aux évolutions des services proposés.
        </p>

        <p>
          Chaque nouvelle version des Conditions Générales d’Utilisation sera
          mise en ligne sur le Site. Tout Visiteur est donc tenu de se référer
          à l’unique version en ligne à la date de sa consultation qui sera
          automatiquement la dernière version desdites CGU.
        </p>

        <p>
          L'accès au site et son utilisation sont réservés à un usage strictement personnel.
          Vous vous engagez à ne pas utiliser ce site et les informations ou données
          qui y figurent à des fins commerciales, politiques, publicitaires et pour
          toute forme de sollicitation commerciale et notamment l'envoi de
          courriers électroniques non sollicités.
        </p>
      </div>

      <div className={classes.section}>
        <h3 className={classes.sectionTitle}>
          Le Contenu et les œuvres sur Flykke
        </h3>

        <div className={classes.subSection}>
          <h4 className={classes.subSectionTitle}>
            a) Propriété du contenu
          </h4>

          <p>
            Par défaut, toutes les marques, photographies, textes, commentaires,
            illustrations, images animées ou non, séquences vidéo, sons,
            ainsi que toutes les applications informatiques qui pourraient
            être utilisées pour faire fonctionner ce site et plus généralement
            tous les éléments reproduits ou utilisés sur le site sont protégés
            par les lois en vigueur au titre de la propriété intellectuelle.
          </p>

          <p>
            Ils sont la propriété pleine et entière de l'éditeur ou de ses partenaires.
            Toute reproduction, représentation, utilisation ou adaptation, sous quelque forme que
            ce soit, de tout ou partie de ces éléments, y compris les
            applications informatiques, sans l'accord préalable et
            écrit de l'éditeur, sont strictement interdites. Le fait pour l'éditeur de ne
            pas engager de procédure dès la prise de connaissance de ces utilisations non
            autorisées ne vaut pas acceptation desdites utilisations et renonciation aux poursuites.
          </p>
        </div>

        <div className={classes.subSection}>
          <h4 className={classes.subSectionTitle}>
            b) Le bouton Flykke (soutenir un projet)
          </h4>

          <p>
            Un Flykke est défini comme l’action de cliquer sur un bouton du même
            nom ou sur un bouton « soutenir ce projet ». Cette action
            entraine l’ouverture d’un encart permettant à l’utilisateur de
            visionner un message publicitaire. Pour que le Flykke soit valide
            l’utilisateur doit regarder le message publicitaire jusqu’à son terme,
            ce qui entraine le versement par l’annonceur d’un montant destiné à
            être reversé à un projet associé au bouton Flykke cliqué.
            Cette action ne créée aucun droit d’aucune sorte au profit de
            l’utilisateur tant face à l’annonceur que face au bénéficiaire que face à l’éditeur.
          </p>
        </div>

        <div className={classes.subSection}>
          <h4 className={classes.subSectionTitle}>
            c) Partage de certains contenus et cession de droits d’auteurs
          </h4>

          <p>
            Certains contenus seront mis à disposition en l’échange d’un Flykke et
            sous certaines conditions. Le titulaire des droits autorise l’exploitation de
            l’œuvre, ainsi que la création d’œuvres dérivées, à condition
            qu’il ne s’agisse pas d’une utilisation commerciale (les utilisations commerciales
            restant soumises à son autorisation). Vous pouvez consulter le détail
            de ces conditions ici : Licence de cession des droits d’auteurs Flykke.
          </p>

          <p>
            En bref : En l’échange d’un Flykke vous faites un don financier au porteur du
            projet « Flykké », de plus, dans certains cas mentionnés seulement,
            vous êtes autorisés à partager et à adapter les œuvres disponibles en
            téléchargement. Cependant vous n’êtes pas autorisés à faire un usage
            commercial de cette œuvre, tout ou partie du matériel la composant.
          </p>
        </div>
      </div>

      <div className={classes.section}>
        <h3 className={classes.sectionTitle}>Gestion du site</h3>

        <p className={classes.preList}>
          Pour la bonne gestion du site, l'éditeur pourra, notamment, à tout moment:
        </p>

        <ul>
          <li>
            Suspendre, interrompre ou limiter l'accès à tout ou partie du site,
            réserver l'accès au site, ou à certaines parties du site,
            à une catégorie déterminée d'internaute ;
          </li>

          <li>
            Supprimer toute information pouvant en perturber le fonctionnement ou
            entrant en contravention avec les lois nationales ou internationales,
            ou avec les règles de l’internet ;
          </li>

          <li>
            Suspendre le site afin de procéder à des mises à jour.
          </li>
        </ul>
      </div>

      <div className={classes.section}>
        <h3 className={classes.sectionTitle}>Responsabilités</h3>

        <p>
          La responsabilité de l'éditeur ne peut être engagée en cas de défaillance,
          panne, difficulté ou interruption de fonctionnement, empêchant l'accès au
          site ou à une de ses fonctionnalités. Le matériel de connexion au site que
          vous utilisez est sous votre entière responsabilité. Vous devez prendre toutes les
          mesures appropriées pour protéger votre matériel et vos propres données
          notamment d'attaques virales par Internet. Vous êtes par ailleurs le seul
          responsable des sites et données que vous consultez.
        </p>

        <p className={classes.preList}>
          L'éditeur ne pourra être tenu responsable en cas de poursuites judiciaires à votre encontre:
        </p>

        <ul>
          <li>
            Du fait de l'usage du site ou de tout service accessible via Internet ;
          </li>

          <li>
            Du fait du non-respect par vous des présentes conditions générales.
          </li>

          <li>
            Suspendre le site afin de procéder à des mises à jour.
          </li>
        </ul>

        <p>
          L'éditeur n'est pas responsable des dommages causés à vous-même,
          à des tiers et/ou à votre équipement du fait de votre connexion ou
          de votre utilisation du site et vous renoncez à toute action contre lui de ce fait.
          Si l'éditeur venait à faire l'objet d'une procédure amiable ou
          judiciaire à raison de votre utilisation du site, il pourra retourner
          contre vous pour obtenir indemnisation de tous les préjudices, sommes,
          condamnations et frais qui pourraient découler de cette procédure.
        </p>

        <p>
          La Société est expressément tenue à une obligation de moyens dans la
          fourniture du service. Aucun conseil et aucune information, qu’ils soient
          oraux ou écrits, obtenus par l’Utilisateur dans le cadre de l’utilisation du
          Site ne sont susceptibles de créer des garanties non expressément
          prévues par les présentes CGU.
        </p>

        <p>
          Les sites et articles qui sont publiés sur le Site ou vers
          lesquels sont créés des liens ne constituent pas des consultations
          juridiques, comptables, commerciales, fiscales ou financières,
          qui supposent l’analyse d’un cas particulier par un professionnel.
        </p>

        <p>
          La Société ne pourra pas être tenue responsable des dommages résultant de
          l’interprétation ou de l’utilisation des informations auxquelles le Site
          donne accès. Toutes les informations données contenues sur ce Site peuvent
          contenir des erreurs techniques et/ou typographiques.
        </p>
      </div>

      <div className={classes.section}>
        <h3 className={classes.sectionTitle}>Cookies</h3>

        <p>
          Le site peut collecter automatiquement des informations standards telles que votre adresse IP.
          <br/>
          Nous collectons également des cookies de personnalisation, afin de vous
          fournir des contenus et publicités susceptibles de vous intéresser en
          fonction de vos centres d’intérêts et préférences.
          Nos cookies vous permettent également de rester connecter (ou déconnectés) sur le site.
          <br/>
          Mais principalement, nos cookies sont analytiques, ils nous servent
          suivre le volume et informer les annonceurs et les bénéficiaires des trafics
          engendrés, le type et la configuration du trafic utilisant ce site, pour en développer
          la conception et l'agencement et à d'autres fins administratives et de
          planification et plus généralement pour améliorer le service que nous vous offrons.
        </p>

        <p>
          Cependant, nous soulignons qu’en aucun cas nous ne partageons ou
          commercialisons vos données personnelles. Pour plus d’informations,
          référez vous à notre page confidentialité.
        </p>
      </div>

      <div className={classes.section}>
        <h3 className={classes.sectionTitle}>
          Sécurité et protection des données personnelles
        </h3>

        <div className={classes.subSection}>
          <h4 className={classes.subSectionTitle}>
            a) Politique de confidentialité
          </h4>

          <p>
            Les informations personnelles pouvant être recueillies sur le
            site sont principalement utilisées par l'éditeur pour la
            gestion des relations avec vous, et le cas échéant pour le
            traitement de vos commandes, dons, utilisation du réseau social.
            Elles sont enregistrées dans le fichier de clients et
            d’utilisateurs de l'éditeur, et le fichier ainsi élaboré à partir
            de données à caractère personnel est déclaré auprès de la CNIL.
          </p>

          <p>
            Conformément aux dispositions de la loi n°78-17 du 6 janvier 1978 modifiée,
            relative à l'informatique, aux fichiers et aux libertés, vous disposez
            d'un droit d'accès, d'interrogation, de modification et de
            suppression des informations qui vous concernent, à exercer à tout moment
            auprès de l'éditeur soit directement sur le site à la rubrique
            « Contactez-nous », soit par courrier postal à l'adresse suivante :
            1, rue Thérèse 75001 Paris, soit par courriel à l'adresse suivante :
            contact@goodeed.com.
          </p>

          <p>
            Pour des raisons de sécurité et afin d’éviter toute fraude, cette demande
            devra être accompagnée d'un justificatif d'identité. Après
            traitement de la demande, ce justificatif sera détruit.
          </p>

          <p>
            Les informations recueillies pourront éventuellement être
            communiquées à des tiers liés à l'éditeur par contrat
            pour l'exécution de tâches sous-traitées nécessaires à la gestion
            de votre compte et sans que vous ayez à donner votre autorisation.
            En cas d'infraction avérée à des dispositions légales ou réglementaires,
            ces informations pourront faire l'objet d'une communication sur demande
            expresse et motivée des autorités judiciaires.
          </p>

          <p>
            Lorsque certaines informations sont obligatoires pour accéder à des
            fonctionnalités spécifiques du site, l'éditeur indiquera ce caractère
            obligatoire au moment de la saisie des données.
            Vous êtes susceptible de recevoir des offres commerciales de l'éditeur.
          </p>

          <p>
            Si vous ne souhaitez pas que vos données soient utilisées par les
            partenaires de l'éditeur à des fins de prospection commerciale,
            veuillez retirer votre adresse email de votre « profil » sur le site.
          </p>

          <p>
            Si, lors de la consultation du site, vous accédez à des données à
            caractère personnel, vous devez vous abstenir de toute collecte,
            de toute utilisation non autorisée et de tout acte pouvant
            constituer une atteinte à la vie privée ou à la réputation des
            personnes. L'éditeur décline toute responsabilité à cet égard.
          </p>
        </div>

        <div className={classes.subSection}>
          <h4 className={classes.subSectionTitle}>
            b) Transparence
          </h4>

          <p>
            Notre objectif est de libérer la créativité et le soutien
            aux causes qui vous sont chères de la contrainte monétaire.
            Pour cela notre politique est de vous permettre d’allouer à
            votre guise les revenus publicitaires que nous générons sur Flykke.
          </p>

          <p>
            Lorsque vous cliquez sur un bouton Flykke et que vous
            validez le visionnage de la publicité, nous nous
            engageons à reverser à l’auteur de l’œuvre ou du
            projet 70% de ce qu’a payé l’annonceur pour cet affichage.
          </p>

          <p>
            Nos partenaires, créateurs comme ONG, s’engagent à utiliser les fonds
            redistribués pour les projets explicités sur Flykke. De plus,
            nos ONG partenaires publient annuellement leurs résultats et
            justifient l’utilisation de leurs fonds.
          </p>

          <p>
            Pour information, les 30% restants sont alloués au développement
            de notre plateforme (principalement pour développer notre offre,
            trouver de nouveaux partenaires et créer des outils plus
            performants dans le but de vous faire profiter d’une
            expérience toujours plus satisfaisante !).
          </p>
        </div>
      </div>

      <div className={classes.section}>
        <h3 className={classes.sectionTitle}>
          Loi applicable
        </h3>

        <p>
          Les présentes conditions d'utilisation du site sont régies par la loi
          française et soumises à la compétence des tribunaux de PARIS.
        </p>
      </div>

      <div className={classes.section}>
        <h3 className={classes.sectionTitle}>
          Résiliation et récupération de vos données personnelles
        </h3>

        <p>
          Vous pouvez supprimer votre compte ou désactiver votre application à tout moment.
          Conformément aux dispositions de la loi n°78-17 du 6 janvier 1978 modifiée,
          relative à l'informatique, aux fichiers et aux libertés, vous disposez
          d'un droit d'accès, d'interrogation, de modification et de suppression
          des informations qui vous concernent, à exercer à tout moment auprès de
          l'éditeur soit directement sur le site, soit par courriel
          à l'adresse suivante :
          <Link className={classes.link} href='mailto:contact@flykke.com' icon='email'>contact@flykke.com</Link>
        </p>
      </div>
    </div>
  </div>
);

export default CGU;
