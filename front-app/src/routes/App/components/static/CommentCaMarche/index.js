import React from 'react';
import classes from '../styles.css';
import Subheader from 'shared/src/components/SubHeader';

const CommentCaMarche = () => (
  <div>
    <Subheader type='centered'>
      <h2 className={classes.SubHeaderTitle}>Comment ça marche ?</h2>
      <div className={classes.IconsWrapper}>
        <div>
          <h4>
            <i className="material-icons">favorite_border</i>
            <br/>
            Choisissez un projet
            </h4>
        </div>

        <div>
          <h4>
            <i className="material-icons">ondemand_video</i>
            <br/>
            Regardez une publicité
          </h4>
        </div>

        <div>
          <h4>
            <i className="material-icons">beenhere</i>
            <br/>
            Votre don est validé
          </h4>
        </div>
      </div>
    </Subheader>

    <div className={classes.ContentWrapper}>

      <div className={classes.SectionHeader}>
        <p>
          Apres que vous vous soyez inscrit sur Flykke vous allez pouvoir
          commencer à soutenir sans limite tous vos créateurs de contenu et ONG
          préférés présent sur le site
        </p>
      </div>

      <div className={classes.section} style={{ marginTop: 30 }}>
        <p>
          A chaque fois que vous cliquez sur un bouton Flykke vous regarderez
          une publicité ciblée en fonction du post flykké. Ainsi vous ne Regardez
          de la publicité que lorsque vous le souhaitez, et ça vous permet de faire un don!
        </p>

        <p>
          Nous nous engageons à reverser à chaque flykke donné 70% de la somme payée
          par l'annonceur au créateur de contenu ou à l'ONG.
          <br/>
          Si la pub que vous regardez a payé 0,10&euro; pour sponsoriser un contenu 0,07&euro; sont reversé
          à l'ONG ou au créateur.
        </p>

        <p>
          Nous voulons construire une expérience de navigation différente où découvrir,
          créer et partager du contenu devient gratuit, sans limite et sans intrusion publicitaire.
          N'hésitez pas à entrer en contact avec nous pour nous aider à améliorer cette expérience
          ou pour participer à cette aventure.
        </p>
      </div>
    </div>
  </div>
);

export default CommentCaMarche
