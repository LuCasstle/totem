import React from 'react';
import classes from '../styles.css';
import Subheader from 'shared/src/components/SubHeader';
import Link from 'react-toolbox/lib/link';

const Confidentialite = () => (
  <div>
    <Subheader type='centered'>
      <h2 style={{ margin: 0 }} className={classes.SubHeaderTitle}>Confidentialité</h2>
    </Subheader>

    <div className={classes.ContentWrapper}>
      <div className={classes.SectionHeader}>
        <p>
          Chez Flykke nous mettons un point d’honneur à ne pas
          divulguer ou commercialiser vos données personnelles.
        </p>

        <p>
          Nous pensons que l’anonymat de nos internautes est une obligation.
          Notre service doit fournir une offre cohérente et efficace pour nos
          clients publicitaires tout en vous permettant de bénéficier
          d’un service puissant, efficace, agréable et respectueux.
        </p>

        <p>
          C’est pourquoi nous garantissons votre anonymat sur Flykke,
          de plus nous ne partageons pas vos informations personnelles
          (bien qu’anonymes) sans votre accord. Nous les utilisons pour
          améliorer notre service de don à destination des donateurs qui l’utilisent.
          En utilisant notre service, vous acceptez l’utilisation et la collecte des
          données qui vous ont été demandées, en accord avec l’utilisation
          décrite dans notre politique de confidentialité.
        </p>

        <p>
          Si vous avez la moindre question à propos notre politique de
          confidentialité ou plus généralement à propos de notre service,
          n’hésitez pas à nous contacter à l’adresse suivante
          {' : '}
          <Link className={classes.link} href='mailto:contact@flykke.com' icon='email'>contact@flykke.com</Link>
        </p>
      </div>

      <div className={classes.section}>
        <h4 className={classes.sectionTitle}>
          Quelles sont les données que nous collectons ?
        </h4>

        <div className={classes.subSection}>
          <h5 className={classes.subSectionTitle}>
            a) Celles auxquelles vous nous permettez d’accéder à l’inscription
          </h5>

          <p>
            En utilisant notre service, nous vous demandons quelques informations.
            Pour vous inscrire, vous avez le choix de vous connecter par Facebook,
            Google ou bien Twitter. Nous avons besoin de cette inscription afin
            de vous identifier comme utilisateur du site et afin de récupérer
            des infos basiques de votre profil (telles que votre date d’anniversaire ou votre géolocalisation).
            Rassurez vous, nous utiliserons uniquement ses données sur notre
            site pour vous permettre de regarder des pubs plus intéressantes
            pour vous et vous fournir un service mieux adapté à vos centres d’intérêts.
          </p>
        </div>

        <div className={classes.subSection}>
          <h5 className={classes.subSectionTitle}>
            b) Données de connexion
          </h5>

          <p>
            Nous collectons quelques informations que nous envoie votre navigateur (“log data”).
            Ces données incluent votre adresse d’Internet Protocol (IP),
            votre type de navigateur (qui peut modifier l’affichage des pages du
            site et implique un affichage différent du graphisme des pages),
            les pages que vous visitez sur notre site, le temps,
            la fréquence et d’autres statistiques sur votre parcours sur le
            site. Une fois de plus, ces quelques données nous
            permettent de mieux comprendre nos utilisateurs afin de
            mieux répondre à leurs attentes et besoins.
          </p>
        </div>

        <div className={classes.subSection}>
          <h5 className={classes.subSectionTitle}>
            c) Données de cookies
          </h5>

          <p>
            Lorsque nous utilisons des cookies, nous pouvons soit utiliser des
            cookies de "session" (qui durent jusqu'à ce que vous fermiez
            votre navigateur) ou des cookies "persistants" (qui restent actifs
            tant qu'ils ne sont pas supprimés par vous-même
            ou par votre navigateur). Nous pouvons par exemple utiliser des cookies
            pour stocker vos préférences de langue ou d'autres paramètres
            propres à Flykke, de sorte que vous n'ayez pas à les
            configurer à nouveau chaque fois que vous accédez à Pinterest.
            Certains cookies sont associés à votre compte Flykke et comprennent des
            informations personnelles vous concernant (l'adresse e-mail
            que vous nous avez indiquée), d’autres non.
          </p>

          <p>
            Ces cookies servent donc soit à personnaliser votre expérience
            sur Flykke, soit à rendre votre navigation plus facile et agréable.
          </p>

          <p>
            Mais principalement, nos cookies sont analytiques, ils nous servent
            suivre le volume et informer les annonceurs et les
            bénéficiaires des trafics engendrés, le type et la configuration
            du trafic utilisant ce site, pour en développer la conception et
            l'agencement et à d'autres fins administratives et de planification
            et plus généralement pour améliorer le service que nous vous offrons.
          </p>

          <p>
            Sachez que vous pouvez choisir de ne pas envoyer de cookies
            en vous rendant dans les paramètres de votre navigateur.
            Cependant, vous risquez de ne pouvoir accéder à tous nos services.
          </p>
        </div>
      </div>

      <div className={classes.section}>
        <h4 className={classes.sectionTitle}>
          Service Providers
        </h4>

        <p>
          Nous pouvons utiliser quelques services ou prestataires externes pour
          faciliter l’utilisation de notre site internet. Ces services
          peuvent avoir accès de manière anonyme et pour un objectif
          non commercial à quelques une de nos données.
        </p>
      </div>

      <div className={classes.section}>
        <h4 className={classes.sectionTitle}>
          Accord avec la Loi
        </h4>

        <p>
          Les données collectées sont déclarées à la CNIL. Nous pouvons être
          amenés à en divulguer une partie selon si nécessaire par
          procédure judiciaire justifiée et dans la raisonnabilité des demandes.
          En cas de demande gouvernementale sur vos données personnelles vous serez notifiés.
        </p>
      </div>

      <div className={classes.section}>
        <h4 className={classes.sectionTitle}>
          Sécurité
        </h4>

        <p>
          L’univers informatique est en mouvance constante, soulevant certaines
          problématiques comme la sécurité de vos données. Pour
          nous il est primordial de vous assurer la sécurité la plus totale.
          Même si nous nous efforçons de préserver la sécurité de votre
          contenu et de votre compte, nous ne pouvons garantir
          que des tiers non autorisés ne parviendront pas à déjouer nos mesures de sécurité.
          Nous vous demandons de maintenir votre mot de passe
          sécurisé et de faire attention à qui vous le communiquez.
          Veuillez nous informer immédiatement de toute utilisation ou
          tentative d'accès non autorisée à votre compte.
        </p>
      </div>

      <div className={classes.section}>
        <h4 className={classes.sectionTitle}>
          Transfert international
        </h4>

        <p>
          Vos informations, incluant vos informations personnelles peuvent être
          transférées à des serveurs localisés ailleurs que dans votre état,
          province ou pays, où les lois de protection des données
          sont différentes que dans votre juridiction. En utilisant
          ce service vous acceptez le transfert international de vos données.
        </p>
      </div>

      <div className={classes.section}>
        <h4 className={classes.sectionTitle}>
          Qui peut utiliser Flykke
        </h4>

        <p>
          Vous pouvez utiliser nos services uniquement si vous acceptez
          d'être lié à Flykke par un contrat, et de vous conformer
          aux présentes conditions et à toutes les lois applicables.
          Lors de la création de votre compte Flykke, vous devez
          nous fournir des renseignements exacts et complets.
          L'utilisation ou l'accès est interdit à toute personne
          âgée de moins de 13 ans. Nous ne serons être tenu responsable
          en cas de collecte d’informations personnelles d’un enfant de
          moins de 13 ans sans le consentement d’un responsable,
          le cas échéant nous prendrons directement les mesures
          pour retirer ces informations de nos serveurs.
        </p>
      </div>

      <div className={classes.section}>
        <h4 className={classes.sectionTitle}>
          Changements de notre politique de confidentialité
        </h4>

        <p>
          Nous pouvons modifier nos politiques de confidentialité,
          nous vous notifierons des changements en publiant les nouvelles
          politiques sur cette page et en affichant la date de dernière
          modification. Il vous est recommandé de lire les modifications
          de notre politique de confidentialité.
        </p>
      </div>
    </div>
  </div>
);

export default Confidentialite
