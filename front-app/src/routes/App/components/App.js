import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Redirect } from 'react-router-dom';
import RouteWithSubRoutes from 'shared/src/components/RouteWithSubRoutes';
import CoreLayout from 'src/layouts/CoreLayout';

class App extends Component {
  static propTypes = {
    loading: PropTypes.bool.isRequired,
    loaded: PropTypes.bool.isRequired,

    getData: PropTypes.func.isRequired,
  }

  componentWillMount() {
    const { loading, loaded, getData } = this.props;
    if (!loading && !loaded) {
      getData()
    }
  }

  render() {
    const { loaded, routes } = this.props;

    if (!loaded) {
      return (
        <CoreLayout><div></div></CoreLayout>
      );
    }

    return (
      <CoreLayout>
        {
          routes.map((route, i) => (
            <RouteWithSubRoutes key={i} {...route}/>
          ))
        }
      </CoreLayout>
    );
  }
}

export default App;
