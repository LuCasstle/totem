import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classes from './styles.css';
import Input from 'react-toolbox/lib/input'
import Button from 'react-toolbox/lib/button';
import { REQUEST_MODEL, REQUEST_STATUSES, FORM_DETAILS } from 'shared/src/constants/models/request.js';
import FormValidator from 'shared/src/components/form/FormValidator';
import ErrorsList from 'shared/src/components/form/ErrorsList';
import history from 'src/store/history';

class RequestForm extends Component {
  static propTypes = {
    requestForm: PropTypes.object.isRequired,
    valid: PropTypes.bool.isRequired,
    errorHash: PropTypes.object.isRequired,

    handleError: PropTypes.func.isRequired,
    updateForm: PropTypes.func.isRequired,
    submitForm: PropTypes.func.isRequired,
  };

  render() {
    const {
      requestForm, errorHash, valid,
      handleError, submitForm, updateForm
    } = this.props;

    return (
      <div className={classes.FormWrapper}>

        <div className={classes.DoubleInputWrapper}>
          <Input
            type='text'
            label='Nom'
            name='Name'
            value={requestForm.get(REQUEST_MODEL.NAME)}
            error={handleError(REQUEST_MODEL.NAME)}
            onChange={value => updateForm(REQUEST_MODEL.NAME, value)}
          />

          <Input
            type='text'
            label='URL'
            name='url'
            value={requestForm.get(REQUEST_MODEL.URL)}
            onChange={value => updateForm(REQUEST_MODEL.URL, value)}
          />
        </div>

        <div className={classes.DoubleInputWrapper}>
          <Input
            type='text'
            label='Prénom contact'
            name='contact_firstname'
            value={requestForm.get(REQUEST_MODEL.CONTACT_FIRSTNAME)}
            onChange={value => updateForm(REQUEST_MODEL.CONTACT_FIRSTNAME, value)}
          />

          <Input
            type='text'
            label='Nom contact'
            name='contact_lastname'
            value={requestForm.get(REQUEST_MODEL.CONTACT_LASTNAME)}
            onChange={value => updateForm(REQUEST_MODEL.CONTACT_LASTNAME, value)}
          />
        </div>

        <div className={classes.DoubleInputWrapper}>
          <Input
            type='text'
            label='Email contact'
            name='contact_email'
            value={requestForm.get(REQUEST_MODEL.CONTACT_EMAIL)}
            error={handleError(REQUEST_MODEL.CONTACT_EMAIL)}
            onChange={value => updateForm(REQUEST_MODEL.CONTACT_EMAIL, value)}
          />

          <Input
            type='text'
            label='Téléphone contact'
            name='contact_phone'
            value={requestForm.get(REQUEST_MODEL.CONTACT_PHONE)}
            onChange={value => updateForm(REQUEST_MODEL.CONTACT_PHONE, value)}
          />
        </div>

        <Input
          type='text'
          label='Information complétementaire'
          multiline
          name='complementary_information'
          value={requestForm.get(REQUEST_MODEL.COMPLEMENTARY_INFORMATION)}
          onChange={value => updateForm(REQUEST_MODEL.COMPLEMENTARY_INFORMATION, value)}
        />

        <ErrorsList errorHash={errorHash} />

        <div className={classes.ActionsWrapper}>
          <Button onClick={() => history.push('/')}>Annuler</Button>

          <Button
            raised type='submit' primary
            disabled={!valid}>
            {'Envoyer'}
          </Button>
        </div>
      </div>
    );
  }
}

export default props => (
  <FormValidator
    FormContent={RequestForm}
    model={REQUEST_MODEL}
    formDetails={FORM_DETAILS}
    formIsClass={true}
    modelInstance={props.requestForm}
    submitForm={props.submitForm}
    afterSubmit={props.afterSubmit}
    className={classes.RequestForm}
    {...props}
  />
);
