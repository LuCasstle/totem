import React from 'react';
import classes from './styles.css';
import Subheader from 'shared/src/components/SubHeader';
import PostRequestForm from './Form';

const PostRequest = (props) => (
  <div>
    <Subheader type='centered'>
      <h2 className={classes.SubHeaderTitle}>
        Vous avez un projet
      </h2>

      <h4>
        Sponsorisez les projets qui correspondent au mieux à votre message publicitaire.
        Permettez à vos clients potentiels de faire des dons gratuitement à leurs ONG et
        créateurs préférés, rentrez dans la nouvelle ère de la publicité en ligne
      </h4>
    </Subheader>

    <div className={classes.ContentWrapper}>
      <div className={classes.SectionHeader}>
        <h4>
          Chaque projet est unique, parlons de vos besoins, <br/>
          nous vous proposerons des solutions sur mesure
        </h4>
      </div>

      <PostRequestForm {...props} />
    </div>
  </div>
);

export default PostRequest;
