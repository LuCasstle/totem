import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Button from 'react-toolbox/lib/button';
import Input from 'react-toolbox/lib/input';
import { OWNER_MODEL } from 'shared/src/constants/models/owner';
import { forEach } from 'lodash';
import styles from './styles.css';

const {
  ADVERTISER_NAME, EMAIL, FIRSTNAME, NAME,
  PHONE, BUDGET, COMPLEMENTARY_INFORMATION
} = OWNER_MODEL;

const requiredFields = [ADVERTISER_NAME, EMAIL];

class AdvertiserForm extends Component {
  state = {
    error: {}
  }

  formIsValid = () => {
    const { form } = this.props;
    const newError = {};

    let valid = true;
    forEach(requiredFields, formKey => {
      if (!form.get(formKey) || form[formKey] === '') {
        valid = false;
        newError[formKey] = 'This field is required';
      }
    });

    this.setState({ error: newError });
    return valid;
  }

  handleSubmit = () => {
    const { form, submitForm } = this.props;

    if (this.formIsValid()) { submitForm(form); }
  }

  render() {
    const { form, updateForm } = this.props;
    const { error } = this.state;

    return (
      <div>
        <div className={styles.SubHeader}>
          <div className={styles.subHeaderSub}>
            <h1 className={styles.SubHeaderTitle}>
              Communiquez avec cohérence dans un format respectueux de votre audience
            </h1>

            <h4>
              Sponsorisez les projets qui correspondent au mieux à votre message publicitaire.
              Permettez à vos clients potentiels de faire des dons gratuitement à leurs ONG et
              créateurs préférés, rentrez dans la nouvelle ère de la publicité en ligne
            </h4>
          </div>
        </div>

        <div className={styles.ContentWrapper}>
          <div className={styles.CommercialBrochure}>
            <span>Envie d'en savoir plus?</span>
            <Button primary>Telechargez notre plaquette commerciale</Button>
          </div>


          <div className={styles.FormWrapper}>
            <h4>
              Chaque projet est unique, parlons de vos besoins, <br/>
              nous vous proposerons des solutions sur mesure
            </h4>

            <form
              onSubmit={e => { e.preventDefault(); this.handleSubmit() }}>

              <div className={styles.DoubleInputWrapper}>
                <Input
                  type='text'
                  label='Nom publicitaire'
                  required
                  value={form.get(ADVERTISER_NAME)}
                  onChange={updateForm.bind(null, ADVERTISER_NAME)}
                  error={
                    ADVERTISER_NAME in error &&
                    <span>Error</span>
                  }
                />
                <Input
                  type='email'
                  label='Email'
                  required
                  name={EMAIL}
                  value={form.get(EMAIL)}
                  onChange={updateForm.bind(null, EMAIL)}
                  error={
                    EMAIL in error &&
                    <span>Error</span>
                  }
                />
              </div>

              <div className={styles.DoubleInputWrapper}>
                <Input
                  type='text'
                  label='Prénom'
                  name={FIRSTNAME}
                  value={form.get(FIRSTNAME)}
                  onChange={updateForm.bind(null, FIRSTNAME)}
                />
                <Input
                  type='text'
                  label='Nom'
                  name={NAME}
                  value={form.get(NAME)}
                  onChange={updateForm.bind(null, NAME)}
                />
              </div>

              <div className={styles.DoubleInputWrapper}>
                <Input
                  type='text'
                  label='Téléphone'
                  name={PHONE}
                  value={form.get(PHONE)}
                  onChange={updateForm.bind(null, PHONE)}
                />
                <Input
                  type='number'
                  label='Budget (&euro;)'
                  name={BUDGET}
                  value={form.get(BUDGET)}
                  onChange={updateForm.bind(null, BUDGET)}
                />
              </div>

              <Input
                type='text'
                label='Informations complémentaires'
                multiline
                name={COMPLEMENTARY_INFORMATION}
                value={form.get(COMPLEMENTARY_INFORMATION)}
                onChange={updateForm.bind(null, COMPLEMENTARY_INFORMATION)}
                maxLength={300}
              />

              <div className={styles.actions}>
                <Button primary
                  onClick={() => this.handleSubmit() }>
                  Envoyer
                </Button>
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

AdvertiserForm.propTypes = {
  form: PropTypes.object.isRequired,
  updateForm: PropTypes.func.isRequired,
  submitForm: PropTypes.func.isRequired,
};

export default AdvertiserForm;
