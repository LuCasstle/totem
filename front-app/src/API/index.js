import { fetch } from 'redux-auth';
import { pushNotification, setGlobalLoading, unsetGlobalLoading } from 'shared/src/actions/notifications';
import { NOTIFICATION_ERROR } from 'shared/src/reducers/notifications';
import { map, forEach, snakeCase } from 'lodash';

const optionalDispatch = (dispatch, options, response) => {
  if (options.DISPLAY_GLOBAL_LOADER) {
    setTimeout(() => dispatch(unsetGlobalLoading()), 1000);
  }

  return response
}

export const checkStatus = (dispatch, response) => {
  if (response.status >= 200 && response.status < 300) {
    return response;
  } else {
    const handleError = jsonRes => {
      dispatch(pushNotification({
        message: (jsonRes && jsonRes.error) ?
                    `Error ${jsonRes.status}: ${jsonRes.error}` :
                    errorMapper(response.status),
        notificationType: NOTIFICATION_ERROR
      }));

      return Promise.reject(jsonRes);
    }

    return parseJSON(response).then(handleError, handleError).catch(logErrorIfDev);
  }
}

export const parseJSON = (response, options = {}) => {
  if (options.noJSON) return response;
  return response.json();
}

const logIfDev = (url, response) => {
  if (__DEV__) {
    console.log(`API CALL to "${url}"`);
    console.log(response);
  }

  return response;
}

export const defaultFetch = (dispatch, url, options = {}) => {
  if (options.DISPLAY_GLOBAL_LOADER) {
    dispatch(setGlobalLoading())
  }

  if (options.body && !options.TRADITIONAL_POST) {
    options.body = JSON.stringify(rubyFormat(options.body));
    if (__DEV__) { console.log('Sent body', options.body); }
  }

  return fetch(url, options)
    .then(res => optionalDispatch(dispatch, options, res))
    .then(res => checkStatus(dispatch, res))
    .then((response) => parseJSON(response, options))
    .then((response) => logIfDev(url, response));
};

export const defaultPost = (dispatch, url, options = {}) => {
  options = {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
    },
    ...options
  };

  return defaultFetch(dispatch, url, options);
}

export const defaultPatch = (dispatch, url, options = {}) => {
  options = {
    method: 'PATCH',
    ...options
  };

  return defaultPost(dispatch, url, options);
}

export const defaultDelete = (dispatch, url, options = {}) => {
  options = {
    method: 'DELETE',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
    },
    ...options
  };

  return defaultFetch(dispatch, url, options);
}

const rubyFormat = (obj={}) => {
  if (obj.constructor === Array) {
    return map(obj, (el) => rubyFormat(el))
  } else if (obj.constructor === Object) {
    let newObj = {}
    forEach(Object.keys(obj), k => {
      newObj[snakeCase(k)] = rubyFormat(obj[k])
    })
    return newObj;
  } else {
    return obj
  }
}

export const generateInURLParams = paramsObj => '?' + map(rubyFormat(paramsObj), (v, k) => `${k}=${v}`).join('&');

const errorMapper = status => {
  switch (status) {
    case 401: return 'You are not authorized to make this request';
    case 404: return 'Resource not found';
    case 502: return 'The serveer encountered an error';
    default: 'There was an error, we will look into this issue ASAP';
  }
}
