import { defaultPost } from './index.js';
import { getAssets } from './assets';
import { getTags } from './tags';
import { getPosts } from './posts';
import { getRequests, getOwners } from './projects';


export const getData = dispatch => {
  return Promise.all([
    getRequests(dispatch),
    getOwners(dispatch),
    getTags(dispatch),
    getPosts(dispatch),
    getAssets(dispatch),
  ]).then(([{requests}, {owners}, {tags}, {posts}, assets]) => ({
    requests,
    owners,
    tags,
    posts,
    assets,
  }))
}

export const postAdvertiserForm = (dispatch, form) => {
  return defaultPost(
    dispatch, __SERVER_ADDR__ + '/advertisers/request', {
      body: { advertiserRequest: form },
      DISPLAY_GLOBAL_LOADER: true
    })
}

export const postAPIRequestForm = (dispatch, request) => {
  return defaultPost(
    dispatch, __SERVER_ADDR__ + '/projects/requests', {
      body: { request },
      DISPLAY_GLOBAL_LOADER: true
    })
}
