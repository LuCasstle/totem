import { defaultFetch } from './index.js';

export const getTags = dispatch => defaultFetch(dispatch, __SERVER_ADDR__ + '/tags')
