import { defaultPatch } from './index.js';

export const subscribeToNewsletter = (dispatch, userId) => defaultPatch(
  dispatch,
  `${__SERVER_ADDR__}/users/${userId}/subscribe_to_newsletter`
)
