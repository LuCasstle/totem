import { defaultFetch } from './index.js';

export const getAssets = dispatch => defaultFetch(dispatch, __SERVER_ADDR__ + '/assets', { DISPLAY_GLOBAL_LOADER: true })
