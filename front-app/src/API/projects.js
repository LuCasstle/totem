import { defaultFetch } from './index.js';

export const getRequests = dispatch => defaultFetch(dispatch, __SERVER_ADDR__ + '/projects/requests');
export const getOwners = dispatch => defaultFetch(dispatch, __SERVER_ADDR__ + '/projects/owners');

export const getProjectData = dispatch => {
  return Promise.all([
    getRequests(),
    getOwners()
  ]).then(([{requests}, {owners}]) => ({ requests, owners }))
}
