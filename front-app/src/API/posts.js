import { defaultFetch, generateInURLParams } from './index.js';

export const getPosts = dispatch => defaultFetch(
  dispatch,
  __SERVER_ADDR__ + '/posts/permitted_index',
  { DISPLAY_GLOBAL_LOADER: true }
)
