const MongoClient = require('mongodb').MongoClient,
      ObjectID = require('mongodb').ObjectID,
      Promise = require('promise'),
      assert = require('assert'),
      debug = require('debug')('app:server')

// Connection URL
const url = 'mongodb://mongo:27017/flykke';

// Use connect method to connect to the Server
const connectToMongo = () => new Promise((resolve, reject) => {
  MongoClient.connect(url, (err, db) => {
    if (err) { return reject(err); }
    resolve(db);
  });
});

const posts = {};
const findPost = (db, postId) => new Promise((resolve, reject) => {
  if (posts[postId]) {
    resolve(posts[postId]);
    return;
  }

  // Get the documents collection
  const collection = db.collection('posts');

  // Find some documents
  collection.findOne({ _id: new ObjectID(postId) }, {}, (err, post) => {
    if (err) return reject(err);
    posts[postId] = post;
    resolve(post);
  });
});

let firstActivePost = null, lastRefresh = null;
const findFirstActivePost = db => new Promise((resolve, reject) => {
  if ((Date.now() - lastRefresh) < (1000 /* ms */ * 60 /* s */ * 60 /* min */ * 24 /* h */) && firstActivePost) {
    lastRefresh = Date.now();
    resolve(firstActivePost);
    return;
  }

  // Get the documents collection
  const collection = db.collection('posts');

  // Find some documents
  collection.find({ active: true }, { "sort": [['created_at','desc']], "limit": 1 }).toArray((err, items) => {
    if (err) return reject(err);
    firstActivePost = items.length > 0 && items[0];
    lastRefresh = Date.now();
    resolve(firstActivePost);
    return;
  });
});

const images = {};
const findImage = (db, imageId) => new Promise((resolve, reject) => {
  if (images[imageId]) {
    resolve(images[imageId]);
    return;
  }

  // Get the documents collection
  const collection = db.collection('images');

  // Find some documents
  collection.findOne({ _id: new ObjectID(imageId) }, {}, (err, image) => {
    if (err) return reject(err);
    images[imageId] = image;
    resolve(image);
  });
});

const findPostWithImage = (db, postId) => findPost(db, postId).then(post => {
  if (post && post.image_id) {
    return findImage(db, post.image_id).then(image => {
      post.image = image;
      return post;
    });
  } else { return post; }
});

const findFirstActivePostWithImage = db => findFirstActivePost(db).then(post => {
  if (post && post.image_id) {
    return findImage(db, post.image_id).then(image => {
      post.image = image;
      return post;
    });
  } else { return post; }
});

module.exports = { connectToMongo, findPostWithImage, findFirstActivePostWithImage };
