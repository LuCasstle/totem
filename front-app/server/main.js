const express = require('express')
const debug = require('debug')('app:server')
const path = require('path')
const webpack = require('webpack')
const webpackConfig = require('../shared/config/webpack.config')
const project = require('../shared/config/project.config')
const compress = require('compression')
const { connectToMongo } = require('./mongo');
const { GlobalMetatagsMiddleware, SpecificPostMiddleware, injectMetaOptions } = require('./meta_tags_middleware.js');

const app = express()

// Apply gzip compression
app.use(compress())

// ------------------------------------
// Apply Webpack HMR Middleware
// ------------------------------------

connectToMongo().then(db => {
  if (project.env === 'development') {
    const compiler = webpack(webpackConfig)

    debug('Enabling webpack dev and HMR middleware')
    app.use(require('webpack-dev-middleware')(compiler, {
      publicPath  : webpackConfig.output.publicPath,
      contentBase : project.paths.client(),
      hot         : true,
      quiet       : project.compiler_quiet,
      noInfo      : project.compiler_quiet,
      lazy        : false,
      stats       : project.compiler_stats,
      headers: {
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Origin, X-Requested-With, Content-Type, Accept"
      }
    }))
    app.use(require('webpack-hot-middleware')(compiler, {
      path: '/__webpack_hmr'
    }))

    // Serve static assets from ~/public since Webpack is unaware of
    // these files. This middleware doesn't need to be enabled outside
    // of development since this directory will be copied into ~/dist
    // when the application is compiled.
    app.use((req, res, next) => {
      if (req.url === '/') { return next(); }
      express.static(project.paths.public())(req, res, next)
    })

    app.use('*', GlobalMetatagsMiddleware(db));
    app.use('/articles/:postId', SpecificPostMiddleware(db));

    // This rewrites all routes requests to the root /index.html file
    // (ignoring file requests). If you want to implement universal
    // rendering, you'll want to remove this middleware.
    app.use('*', function (req, res, next) {
      const filename = path.join(compiler.outputPath, 'index.html')
      compiler.outputFileSystem.readFile(filename, (err, result) => {
        if (err) {
          return next(err)
        }
        res.set('content-type', 'text/html')
        res.send(injectMetaOptions(result.toString(), res));
        res.end()
      })
    })
  } else {
    debug(
      'Server is being run outside of live development mode, meaning it will ' +
      'only serve the compiled application bundle in ~/dist. Generally you ' +
      'do not need an application server for this and can instead use a web ' +
      'server such as nginx to serve your static files. See the "deployment" ' +
      'section in the README for more information on deployment strategies.'
    )

    // Serving ~/dist by default. Ideally these files should be served by
    // the web server and not the app server, but this helps to demo the
    // server in production.
    app.use((req, res, next) => {
      if (req.url === '/') { return next(); }
      express.static(project.paths.dist())(req, res, next);
    })

    app.use('*', GlobalMetatagsMiddleware(db));
    app.use('/articles/:postId', SpecificPostMiddleware(db));
    const fs = require('fs');

    app.use('*', function (req, res, next) {
      const filename = path.join(project.paths.dist(), 'index.html')
      fs.readFile(filename, (err, result) => {
        if (err) {
          return next(err)
        }
        res.set('content-type', 'text/html')
        res.send(injectMetaOptions(result.toString(), res));
        res.end()
      })
    })
  }
});

module.exports = app
