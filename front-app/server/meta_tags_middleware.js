const { findPostWithImage, findFirstActivePostWithImage } = require('./mongo');
const Meta = require('express-metatag');
const OGMeta =  Meta('og')
const debug = require('debug')('app:server')
const { startsWith, merge, keys } = require('lodash');
const project = require('../shared/config/project.config')

const GlobalMetatagsMiddleware = db => (req, res, next) => {
  if (
    startsWith(req.baseUrl, '/assets') ||
    startsWith(req.baseUrl, '/articles/')
  ) {
    next(); return;
  }

  findFirstActivePostWithImage(db).then(post => {
    if (post) {
      res._metas = merge(res._metas || {}, {
        'og:url': 'http://' + SERVER_URL + req.baseUrl,
        'og:title': post.title,
        'og:type': 'article',
        'og:description': post.description,

        'twitter:card': 'summary_large_image',
        'twitter:siter': '@Flykke_Officiel',
        'twitter:title': post.title,
        'twitter:description': post.description,
      });
    }

    if (post && post.image) {
      let imageInfo = post.image.src_info;

      res._metas = merge(res._metas || {}, {
        'og:image': imageInfo.url,
        'og:image:height': imageInfo.height,
        'og:image:width': imageInfo.width,

        'twitter:image': imageInfo.url,
        'twitter:image:alt': imageInfo.name,
      });
    }

    next();
  }, err => {
    next(err);
  });
}

const SpecificPostMiddleware = db => (req, res, next) => {
  findPostWithImage(db, req.params.postId).then(post => {
    if (post) {
      res._metas = merge(res._metas || {}, {
        'description': post.description,

        'og:url': 'http://' + SERVER_URL + req.baseUrl,
        'og:title': post.title,
        'og:type': 'article',
        'og:description': post.description,

        'twitter:card': 'summary_large_image',
        'twitter:siter': '@Flykke_Officiel',
        'twitter:title': post.title,
        'twitter:description': post.description,
      });
    }

    if (post && post.image) {
      let imageInfo = post.image.src_info;

      res._metas = merge(res._metas || {}, {
        'og:image': imageInfo.url,
        'og:image:height': imageInfo.height,
        'og:image:width': imageInfo.width,

        'twitter:image': imageInfo.url,
        'twitter:image:alt': imageInfo.name,
      });
    }

    next();
  }, err => {
    next(err);
  });
}

const buildTags = metas => {
  let tags = '';

  for (let meta of keys(metas)) {
    tags += '<meta property="'+ meta + '" content="' + metas[meta] + '">\n';
  }

  return tags;
};

const injectMetaOptions = (str, res) => {
  const metas = res._metas || {};
  delete res._metas;
  return str.replace(/<head>((?:.|\n|\r)+?)<\/head>/i, "<head>$1" + buildTags(metas) + "</head>");
};

module.exports = { GlobalMetatagsMiddleware, SpecificPostMiddleware, injectMetaOptions };

const setServerUrl = () => {
  switch(project.env) {
    case 'development': return 'app.demo.flykke.com';
    case 'staging': return 'app.staging.flykke.com';
    case 'production': return 'app.production.flykke.com';
    default: return 'app.production.flykke.com';
  }
}

let SERVER_URL = setServerUrl();
